<?php

use Engine\Core\Router\Route;
use Engine\Helper\Di;



$route = new Route(Di::di());
$route->mixed("/secure.getAccessToken", "AuthController:getAccessToken", "DATA");
$route->mixed("/secure.checkToken", "AuthController:checkToken", "DATA");
