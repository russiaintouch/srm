<?php

use Engine\Core\Router\Route;
use Engine\Helper\Di;



$route = new Route(Di::di());

$route->get("/", "IndexController:index", "DATA");
$route->get("/index", "IndexController:index", "DATA");


$route->post("/account.registration", "RegistrationController:registration", "DATA");
$route->mixed("/account.confirmation", "RegistrationController:confirmation", "DATA");
$route->mixed("/account.getProfileInfo", "AccountController:getProfileInfo", "DATA");
$route->mixed("/account.saveProfileInfo", "AccountController:saveProfileInfo", "DATA");

$route->get("/product.getProductType", "ProductController:getProductType", "DATA");
$route->get("/product.getProducts", "ProductController:getProducts", "DATA");
$route->mixed("/product.add", "ProductController:add", "DATA");
$route->get("/product.delete", "ProductController:delete", "DATA");
$route->get("/product.getById", "ProductController:getById", "DATA");

$route->get("/extras.get", "ExtrasController:get", "DATA");
$route->mixed("/extras.add", "ExtrasController:add", "DATA");

$route->mixed("/discount.get", "DiscountController:get", "DATA");
$route->mixed("/discount.add", "DiscountController:add", "DATA");
$route->mixed("/discount.getById", "DiscountController:getById", "DATA");
$route->mixed("/discount.edit", "DiscountController:edit", "DATA");
$route->mixed("/discount.delete", "DiscountController:delete", "DATA");

$route->mixed("/category.add", "CategoryController:add", "DATA");
$route->get("/category.get", "CategoryController:get", "DATA");
$route->get("/category.getById", "CategoryController:getById", "DATA");
$route->get("/category.delete", "CategoryController:delete", "DATA");



$route->get("/secure.checkToken", "SecureController:checkToken", "DATA");
$route->get("/secure.token", "SecureController:token", "DATA");

