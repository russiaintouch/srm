<?php

namespace App\Http\Controller\Api;



use Engine\Controller\ApiController;
use Engine\Core\Passport\Permission\Action;
use Engine\Core\Response\ResponseSchema;
use Engine\DI\DI;
use Engine\Foundation\Middleware\Verification;
use Engine\Helper\Message;
use Exception;
use RestCode;



/**
 * Class RegistrationController
 * @package App\Http\Controller\Supplier\Supplier
 */
class AccountController extends ApiController
{


    /**
     * RegistrationController constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->getMiddleware()->execute(new Verification);
    }



    /**
     * Returns information about the current profile.
     * @throws Exception
     * @api /account.getProfileInfo
     */
    public function getProfileInfo()
    {
        $user = $this->getUserRepository()->getUserByEmail($this->payload()->login());
        if (!is_object($user)) {
            Message::error(
                $this->lpm->translate("error", "user_does_not_exist"),
                RestCode::USER_DOES_NOT_EXIST
            );
        }
        $rs = new ResponseSchema();
        $rs->setData($user);
        $json = $this->serializer->serialize($rs, "json", ["attributes" => [
            "code",
            "data" => [
                "id",
                "firstName",
                "lastName",
                "phone",
                "email",
                "image",
                "group" => [
                    "id",
                    "role" => [
                        "id",
                        "name",
                        "permissions",
                    ],
                    "name",
                ],
                "createAt" => [
                    "timestamp"
                ]
            ]
        ]]);
        $this->response->jsonSend($json);
    }



    public function saveProfileInfo()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::EDIT);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "first_name" => "required|max:255",
            "last_name" => "required|max:255",
            "email" => "required|email",
            "phone" => "required|max:15",
            "password" => "min:" . env("MIN_LENGTH_PASSWORD") . "|max:" . env("MAX_LENGTH_PASSWORD"),
            "confirm_password" => "same:password",
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        $user = $this->getUserRepository()->getUserByEmail($this->payload()->login());

        if (!is_object($user)) Message::error($this->translate("user", "user_does_not_exist"), RestCode::USER_DOES_NOT_EXIST) ;

        $user->setFirstName($this->request->mixed("first_name"));
        $user->setLastName($this->request->mixed("last_name"));
        $user->setEmail($this->request->mixed("email"));
        $user->setPhone($this->request->mixed("phone"));

        $user->setPassword(password_hash($this->request->mixed("password"), PASSWORD_BCRYPT));

        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (Exception $e) {
            Message::error(
                $this->translate("error", "err_unknown_error"),
                RestCode::ERR_UNKNOWN_ERROR);
        }
        Message::success($this->translate("user", "user_successfully_updated"));
    }

}
