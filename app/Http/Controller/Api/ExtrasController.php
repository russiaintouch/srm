<?php

namespace App\Http\Controller\Api;


use App\Model\Extras\Extras;
use App\Model\Extras\ExtrasRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Engine\Controller\ApiController;
use Engine\Core\Passport\Permission\Action;
use Engine\Core\Response\ResponseSchema;
use Engine\DI\DI;
use Engine\Foundation\Middleware\Verification;
use Engine\Helper\Message;
use Exception;



/**
 * Class ExtrasController
 * @package App\Http\Controller\Supplier
 */
class ExtrasController extends ApiController
{


    /** @var ExtrasRepository */
    private $extrasRepository;



    /**
     * ExtrasController constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->getMiddleware()->execute(new Verification);
        $this->extrasRepository = new ExtrasRepository($this->em);
    }



    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::ADD);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'name' => 'required|max:200',
            'price' => 'required',
            'description' => 'required|max:1000',
        ]);

        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }

        $exr = new Extras();
        $exr->setName($this->request->mixed("name"));
        $exr->setPrice($this->request->mixed("price", 0));
        $exr->setDescription($this->request->mixed("description", null));
        $exr->setLimitation($this->request->mixed("limitation", 2));
        $exr->setImage($this->request->mixed("image", "*"));

        $this->em->persist($exr);
        $this->em->flush();

        Message::success("Сохранил успешно!");
    }



    public function get()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);

        $count = $this->request->mixed("count", 30);
        $offset = $this->request->mixed("offset", 0);
        $rs = new ResponseSchema();
        $rs->setCount($this->extrasRepository->getCount());
        $rs->setItems($this->extrasRepository->get($count, $offset));
        $json = $this->serializer->serialize($rs, 'json', ['attributes' => [
            "code",
            "count",
            "items"
        ]]);
        $this->response->jsonSend($json);
    }


}
