<?php

namespace App\Http\Controller\Api;



use App\Model\Extras\Extras;
use App\Model\Extras\ExtrasRepository;
use App\Model\Image\Image;
use App\Model\Image\ImageRepository;
use App\Model\Product\Product;
use App\Model\Product\ProductRepository;
use App\Model\ProductType\ProductTypeRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Engine\Controller\ApiController;
use Engine\Core\Passport\Permission\Action;
use Engine\Core\Response\ResponseSchema;
use Engine\DI\DI;
use Engine\Foundation\Middleware\Verification;
use Engine\Helper\Message;
use Exception;



/**
 * Class ProductController
 * @package App\Http\Controller\Supplier
 */
class ProductController extends ApiController
{


    /** @var ProductTypeRepository */
    private $productTypeRepository;

    /** @var ProductRepository */
    private $productRepository;

    /** @var ExtrasRepository */
    private $extrasRepository;

    /** @var ImageRepository */
    private $imageRepository;



    /**
     * ProductController constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->getMiddleware()->execute(new Verification);
        $this->productTypeRepository = new ProductTypeRepository($this->em);
        $this->productRepository = new ProductRepository($this->em);
        $this->extrasRepository = new ExtrasRepository($this->em);
        $this->imageRepository = new ImageRepository($this->em);
    }



    public function add()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::ADD);
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'name' => 'required|max:200',
            'product_type' => 'required|numeric',
            'product_pricing' => 'required',
            'count_min' => 'default:1|numeric',
            'count_max' => 'default:1|numeric',
            'short_description' => 'required',
            'long_description' => 'required',
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        $product_type = $this->productTypeRepository->getById($this->request->mixed("product_type"));
        $product = new Product();
        $product->setProductType($product_type);
        $product->setName($this->request->mixed("name"));
        $product->setUniqueCode($this->request->mixed("unique_code", rand()));
        $product->setProductPricing($this->request->mixed("product_pricing"));
        $product->setAdvertisedPrice((float)$this->request->mixed("advertised_price", 0));
        $product->setCountMin($this->request->mixed("count_min"));
        $product->setCountMax($this->request->mixed("count_max"));
        $product->setShortDescription($this->request->mixed("short_description"));
        $product->setLongDescription($this->request->mixed("long_description"));
        $product->setQuantityLabel($this->request->mixed("quantity_label", '{"singular":"Пассажир","plural":"Пассажиры"}'));
        $product->setOptions($this->request->mixed("options", "[]"));
        $product->setTermsUse($this->request->mixed("terms_use", "null"));
        // images
        $images_criteria = explode(",", $this->request->mixed("images", ""));
        /** @var Image $image */
        foreach ($this->imageRepository->findBy(["id" => $images_criteria]) as $image) {
            $product->addImage($image);
        }
        // extras
        $extras_criteria = explode(",", $this->request->mixed("extras"));
        /** @var Extras $item */
        foreach ($this->extrasRepository->findBy(["id" => $extras_criteria]) as $item) {
            $product->getExtras()->add($item);
        }
        try {
            $this->em->persist($product);
            $this->em->flush();
            Message::success(
                $this->translate("product", "product_successfully_added")
            );
        } catch (OptimisticLockException $e) {
        } catch (ORMException $e) {
        }
    }



    public function delete()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::DELETE);
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'id' => 'required|numeric'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        $this->productRepository->delete($this->request->mixed("id"));
        Message::success(
            $this->translate("product", "product_successfully_deleted")
        );
    }



    public function getById()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'id' => 'required|numeric|min:1'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        /** @var Product $product */
        $product = $this->productRepository->getById($this->request->mixed("id"));
        if (!$product) Message::warning("Данные не существуют!");
        $rs = new ResponseSchema();
        $rs->setData($product);
        $json = $this->serializer->serialize($rs, 'json', ['attributes' => [
            "code",
            "data" => [
                "id",
                "name",
                "uniqueCode",
                "advertisedPrice",
                "countMin",
                "countMax",
                "quantityLabel",
                "longDescription",
                "shortDescription",
                "extras",
                "productType" => [
                    "id",
                    "name",
                    "description",
                ],
                "category",
                "options"
            ]]]);
        $this->response->jsonSend($json);
    }



    /**
     * @throws NonUniqueResultException
     */
    public function getProducts()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);

        $count = $this->request->mixed("count", 30);
        $offset = $this->request->mixed("offset", 0);
        $rs = new ResponseSchema();
        $rs->setCount($this->productRepository->getCount());
        $rs->setItems($this->productRepository->get($count, $offset));
        $json = $this->serializer->serialize($rs, 'json', ['attributes' => [
            "code",
            "count",
            "items" => [
                "id",
                "name",
            ]
        ]]);
        $this->response->jsonSend($json);
    }



    /**
     * @throws NonUniqueResultException
     */
    public function getProductType()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);
        $count = $this->productTypeRepository->getCount();
        $items = $this->productTypeRepository->getItems(
            $this->request->mixed("count", 30),
            $this->request->mixed("offset", 0)
        );
        $rs = new ResponseSchema();
        $rs->setCount($count);
        $rs->setItems($items);
        $json = $this->serializer->serialize($rs, 'json', ['attributes' => [
            "code",
            "count",
            "items"
        ]]);
        $this->response->jsonSend($json);
    }
}
