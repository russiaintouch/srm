<?php

namespace App\Http\Controller\Api;



use App\Model\Category\Category;
use App\Model\Category\CategoryRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Engine\Controller\ApiController;
use Engine\Core\Passport\Permission\Action;
use Engine\Core\Response\ResponseSchema;
use Engine\DI\DI;
use Engine\Foundation\Middleware\Verification;
use Engine\Helper\Message;
use Exception;



/**
 * Class CategoryController
 * @package App\Http\Controller\Supplier
 */
class CategoryController extends ApiController
{


    /** @var CategoryRepository */
    private $categoryRepository;



    /**
     * CategoryController constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->getMiddleware()->execute(new Verification);
        $this->categoryRepository = new CategoryRepository($this->em);
    }



    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::ADD);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "name" => "required|max:200",
            "description" => "max:1000",
            "rel_canonical_url" => "url:http,https",
            "seo_description" => "max:200",
            "indexing" => "in:0,1",
            "visibility" => "in:0,1",
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        $category = new Category();
        $category->setName($this->request->mixed("name"));
        $category->setDescription($this->request->mixed("description"));
        $category->setRelCanonicalURL($this->request->mixed("rel_canonical_url"));
        $category->setSEODescription($this->request->mixed("seo_description"));
        $category->setIndexing($this->request->mixed("indexing"));
        $category->setVisibility($this->request->mixed("visibility"));
        $this->em->persist($category);
        $this->em->flush();
        Message::success($this->translate("category", "category_successfully_added"));
    }



    public function get()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);

        $rs = new ResponseSchema();
        $rs->setCount($this->categoryRepository->count([]));
        $rs->setItems($this->categoryRepository->get(
            $this->request->mixed("offset", 0),
            $this->request->mixed("count", 30)
        ));
        $json = $this->serializer->serialize($rs, "json", ["attributes" => [
            "code",
            "count",
            "items" => [
                "id",
                "name",
                "visibility",
                "indexing",
            ]
        ]]);
        $this->response->jsonSend($json);
    }



    public function getById()
    {
        $this->checkPermissionGet();
        $rs = new ResponseSchema();
        $rs->setData($this->categoryRepository->getById($this->request->get("id")));
        $json = $this->serializer->serialize($rs, "json", ["attributes" => [
            "code",
            "count",
            "data"
        ]]);
        $this->response->jsonSend($json);
    }



    public function delete()
    {
        $this->checkPermissionDelete();
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "id" => "required|numeric",
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        try {
            $this->categoryRepository->delete($this->request->mixed("id"));
            Message::success($this->translate("category", "category_successfully_deleted"));
        } catch (Exception $e) {
            Message::error($this->translate("category", "could_not_delete_category"));
        }
    }
}
