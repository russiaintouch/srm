<?php

namespace App\Http\Controller\Api;



use App\Model\User\User;
use DateTime;
use Doctrine\ORM\ORMException;
use Engine\Controller\ApiController;
use Engine\DI\DI;
use Engine\Helper\Message;
use Exception;
use RestCode;



class RegistrationController extends ApiController
{


    private $userRepository;



    /**
     * RegistrationController constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
    }



    /**
     * Registration
     * ------------------------------
     * @throws Exception
     */
    public function registration()
    {

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'first_name' => 'max:255',
            'last_name' => 'max:255',
            'phone' => 'required|max:30',
            'email' => 'required|email:255',
            'password' => sprintf('required|min:%d|max:%d', env('MIN_LENGTH_PASSWORD', '6'), env('MAX_LENGTH_PASSWORD', '48')),
            'confirm_password' => 'required|same:password',
        ]);
        $validation->setMessage('first_name', $this->lpm->translate('message', 'required_first_name'));
        $validation->setMessage('last_name', $this->lpm->translate('message', 'required_last_name'));
        $validation->setMessage('email', $this->lpm->translate('error', 'invalid_pattern_email'));
        $validation->setMessage('password', $this->lpm->translate('error', 'invalid_pattern_password'));
        $validation->setMessage('confirm_password', $this->lpm->translate('error', 'passwords_do_not_match'));
        $validation->validate();
        if ($validation->fails()) {

            $this->error(
                $this->lpm->translate('error', 'invalid_data'),
                RestCode::ERR_INVALID_DATA,
                ["errors" => $validation->errors()->firstOfAll()]
            );
        }
        if ($this->userRepository->UserExists($this->request->mixed('email'))) {
            $this->warning(
                $this->lpm->translate('warning', 'user_already_exists'),
                RestCode::USER_ALREADY_EXISTS
            );
        }
        $user = new User();
        $user->setCreateAt(new DateTime('now'))
            ->setEmail($this->request->mixed('email'))
            ->setFirstName($this->request->mixed('first_name'))
            ->setLastName($this->request->mixed('last_name'))
            ->setPhone($this->request->mixed('phone'))
            ->setPassword(password_hash($this->request->mixed('password'), PASSWORD_BCRYPT));
        try {
            $this->em->persist($user);
            $this->em->flush();
            $code_confirm = numGenerator(4);
            $this->sendEmailConfirm($this->request->mixed('email'), $code_confirm);
            Message::success($this->lpm->translate('message', 'send_email_confirm'));

        } catch (ORMException $e) {
            Message::error($this->lpm->translate('message', 'send_email_confirm'));
        }
    }



    /**
     *
     * @return mixed
     * @throws Exception
     */
    public function confirmation()
    {
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'code' => 'required|min:4|max:4',
            'email' => 'required|email|max:255'
        ]);
        $validation->setMessage('code', $this->lpm->translate(MSG_LEVEL_WARNING, 'invalid_confirm_code'));
        $validation->validate();
        if ($validation->fails()) {
            Message::error(
                $this->lpm->translate(MSG_LEVEL_WARNING, 'invalid_confirm_code'),
                RestCode::INVALID_CONFIRM_CODE,
                ["errors" => $validation->errors()->firstOfAll()]
            );
        }
        if ($this->getUserRepository()->UserExists($this->request->mixed('email'))) {

            /** @var User $user */
            $user = $this->getUserRepository()->getUserByEmail($this->request->mixed('email'));
            $user->setConfirmed(true);
            try {
                $this->em->persist($user);
                $this->em->flush();
                Message::success($this->lpm->translate('message', 'account_registration_is_confirmed'));

            } catch (Exception $e) {
                Message::error($this->lpm->translate('message', 'there_was_an_error_verifying_your_account'));
            }
        }
    }
}
