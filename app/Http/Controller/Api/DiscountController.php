<?php

namespace App\Http\Controller\Api;


use App\Model\Discount\Discount;
use App\Model\Discount\DiscountRepository;
use Doctrine\ORM\ORMException;
use Engine\Controller\ApiController;
use Engine\Core\Passport\Permission\Action;
use Engine\Core\Response\ResponseSchema;
use Engine\DI\DI;
use Engine\Foundation\Middleware\Verification;
use Engine\Helper\Message;
use Exception;



/**
 * Class DiscountController
 * @package App\Http\Controller\Supplier
 */
class DiscountController extends ApiController
{


    /**
     * @var DiscountRepository
     */
    private $DiscountRepository;



    /**
     * DiscountController constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->middleware->execute(new Verification);
        $this->DiscountRepository = new DiscountRepository($this->em);
    }



    public function get()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);

        $rs = new ResponseSchema();
        $rs->setCount($this->DiscountRepository->count([]));
        $rs->setItems($this->DiscountRepository->get(
            $this->request->mixed("count", 10),
            $this->request->mixed("offset", 0)));
        $json = $this->serializer->serialize($rs, 'json', ['attributes' => [
            "code",
            "count",
            "items"
        ]]);
        $this->response->jsonSend($json);
    }



    /**
     * @throws Exception
     */
    public function add()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::ADD);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "code" => "required"
        ]);

        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }

        $discount = new Discount();
        $discount->setCode($this->request->mixed("code"));
        $discount->setValidityFrom(utsToDateTime($this->request->mixed("validityFrom")));
        $discount->setValidityTo(utsToDateTime($this->request->mixed("validityTo")));
        $discount->setInternalNotes($this->request->mixed("internalNotes"));
        $discount->setMinAmount($this->request->mixed("minAmount"));
        $discount->setApplicability($this->request->mixed("applicability"));
        $discount->setTaxesFees(stringToBoolean($this->request->mixed("taxesFees")));
        $discount->setLimiting($this->request->mixed("limiting"));
        $discount->setStatus(true);

        try {
            $this->em->persist($discount);
            $this->em->flush();

            Message::success(
                $this->translate("discount", "promo_code_successfully_added")
            );

        } catch (ORMException $e) {
            Message::error("Ошибка обновления данных!");
        }
    }



    /**
     * @throws Exception
     */
    public function edit()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::EDIT);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "id" => "required|min:1|numeric"
        ]);

        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }

        /** @var Discount $discount */
        $discount = $this->DiscountRepository->getById(
            $this->request->mixed("id", 10));

        $discount->setCode($this->request->mixed("code"));
        $discount->setValidityFrom(utsToDateTime($this->request->mixed("validityFrom")));
        $discount->setValidityTo(utsToDateTime($this->request->mixed("validityTo")));
        $discount->setInternalNotes($this->request->mixed("internalNotes"));
        $discount->setMinAmount($this->request->mixed("minAmount"));
        $discount->setApplicability($this->request->mixed("applicability"));
        $discount->setTaxesFees(stringToBoolean($this->request->mixed("taxesFees")));
        $discount->setLimiting($this->request->mixed("limiting"));
        $discount->setStatus(stringToBoolean($this->request->mixed("status")));

        try {
            $this->em->persist($discount);
            $this->em->flush();

            Message::success(
                $this->translate("discount", "promo_code_successfully_updated")
            );
        } catch (ORMException $e) {
            Message::error("Ошибка обновления данных!");
        }
    }



    public function getById()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::GET);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'id' => 'required|min:1|numeric'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }

        $rs = new ResponseSchema();
        $rs->setData($this->DiscountRepository->getById(
            $this->request->mixed("id", 10)));
        $json = $this->serializer->serialize($rs, 'json', ['attributes' => [
            "code",
            "data"
        ]]);
        $this->response->jsonSend($json);
    }



    public function delete()
    {
        $this->getPassport()->permission()->checkPermission($this->getRole($this->payload()->login()), Action::DELETE);

        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'id' => 'required|min:1|numeric'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        $this->DiscountRepository->delete(
            $this->request->mixed("id")
        );
        Message::success(
            $this->translate(
                "discount",
                "promo_code_successfully_deleted"
            ));
    }
}
