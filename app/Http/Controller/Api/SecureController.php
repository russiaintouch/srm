<?php


namespace App\Http\Controller\Api;

use App\Model\User\User;
use Engine\Controller\ApiController;
use Engine\ExceptionApi;
use Engine\Helper\Message;
use Engine\Helper\Server;
use Exception;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\HS512;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use RestCode;



/**
 * Class JWTController
 * @package App\Http\Controller\Supplier
 */
class SecureController extends ApiController
{



    /**
     * @throws Exception
     */
    public function checkToken()
    {
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'token' => 'required|max:1000',
            'ip'    => 'required|ipv4',
        ]);

        $validation->setMessage('email', $this->translate('error', 'invalid_pattern_email'));
        $validation->setMessage('ip', $this->translate('error', 'invalid_pattern_ip'));

        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }

        try {
            // The algorithm manager with the HS256 algorithm.
            $algorithmManager = AlgorithmManager::create([new HS512(),]);

            // We instantiate our JWS Verifier.
            $jwsVerifier = new JWSVerifier($algorithmManager);

            // Our key.
            $jwk = new JWK([
                'kty' => 'oct',
                'k' => env("SECRET_KEY")
            ]);

            // The JSON Converter.
            $jsonConverter = new StandardConverter();

            // The serializer manager. We only use the JWS Compact Serialization Mode.
            $serializerManager = JWSSerializerManager::create([new CompactSerializer($jsonConverter),]);

            // We try to load the token.
            $jws = $serializerManager->unserialize($this->request->mixed("token"));

            // We verify the signature. This method does NOT check the header.
            // The arguments are:
            // - The JWS object,
            // - The key,
            // - The index of the signature to check. See
            $isVerified = $jwsVerifier->verifyWithKey($jws, $jwk, 0);
            if ($isVerified){
                $jws_obj = json_decode($jws->getPayload());

                Message::success('OK', [
                    "exp" => $jws_obj->exp,
                    "nbf" => $jws_obj->nbf,
                ]);
            }



        } catch (Exception $e) {
            return false;
        }
    }




    /**
     * Получить access_token
     * @throws Exception
     */
    public function getAccessToken()
    {
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:' . env('MIN_LENGTH_PASSWORD') . '|max:' . env('MAX_LENGTH_PASSWORD'),
        ]);

        $validation->setMessage('email', $this->lpm->translate('error', 'invalid_pattern_email'));
        $validation->setMessage('password', $this->lpm->translate('error', 'invalid_pattern_password'));

        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }

        /** @var User $user */
        $user = $this->getUserRepository()->getUserByEmail($this->request->mixed("email"));

        if (is_object($user)) {

            try {

                if (!$user->isConfirmed()){
                    Message::warning(
                        $this->lpm->translate('message', 'account_verification_required'),
                        RestCode::ACCOUNT_VERIFICATION_REQUIRED);
                }


                /////////////////
                ///

                // The algorithm manager with the HS256 algorithm.
                $algorithmManager = AlgorithmManager::create([
                    new HS512(),
                ]);

                // Our key.
                $jwk = new JWK([
                    'kty' => 'oct',
                    'k' => env('SECRET_KEY')
                ]);

                // The JSON Converter.
                $jsonConverter = new StandardConverter();

                // We instantiate our JWS Builder.
                $jwsBuilder = new JWSBuilder(
                    $jsonConverter,
                    $algorithmManager
                );

                // The payload we want to sign. The payload MUST be a string hence we use our JSON Converter.
                $payload = $jsonConverter->encode([
                    'jti' => '12345',
                    'iat' => time(),
                    'nbf' => time(),
                    'exp' => time() + 24*60*60,
                    'iss' => Server::getServerName(),
//                    'aud' => 'Your application',
                    'email' => $user->getEmail(),
                    'id' => $user->getId()
                ]);

                $jws = $jwsBuilder
                    ->create()                               // We want to create a new JWS
                    ->withPayload($payload)                  // We set the payload
                    ->addSignature($jwk, ['alg' => 'HS512']) // We add a signature with a simple protected header
                    ->build();                               // We build it

                $serializer = new CompactSerializer($jsonConverter); // The serializer
                $token = $serializer->serialize($jws, 0);

                Message::success(
                    $this->translate('message', 'successful_authorization'),
                    ['access_token' => $token]
                );


            } catch (Exception $e) {
                throw new ExceptionApi('error', $e->getMessage(), 'error', -1, $e->getPrevious() );
            }
        } else {
            Message::error(
                $this->lpm->translate('message', 'err_user_authorization_failed'),
                RestCode::ERR_USER_AUTHORIZATION_FAILED
                );
        }
    }
}
