<?php

namespace App\Http\Controller\Auth;



use App\Model\User\User;
use Engine\Controller\ApiController;
use Engine\Core\Passport\JWT;
use Engine\Core\Passport\JWT\PayloadBuilder;
use Engine\ExceptionApi;
use Engine\Helper\Message;
use Exception;
use RestCode;



class AuthController extends ApiController
{


    Use JWT;



    /**
     * @throws Exception
     */
    public function checkToken()
    {
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "token" => "required|max:500",
        ]);
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        try {

            $token = $this->request->mixed("token");
            if ($this->getPassport()->verification($token)) {
                $payload = $this->getPassport()->payload($token);
                Message::success("OK", [
                    "exp" => $payload->exp(),
                    "nbf" => $payload->nbf(),
                ]);
            }


        } catch (Exception $e) {
            return false;
        }
    }



    /**
     * Получить access_token
     * @throws Exception
     */
    public function getAccessToken()
    {
        $validator = $this->getValidator();
        $validation = $validator->make($this->request->request, [
            "email" => "required|email|max:255",
            "password" => "required|min:" . env("MIN_LENGTH_PASSWORD") . "|max:" . env("MAX_LENGTH_PASSWORD"),
        ]);
        $validation->setMessage("email", $this->lpm->translate("error", "invalid_pattern_email"));
        $validation->setMessage("password", $this->lpm->translate("error", "invalid_pattern_password"));
        $validation->validate();
        if ($validation->fails()) {
            Message::msgInvalidData($validation->errors()->firstOfAll());
        }
        /** @var User $user */
        $user = $this->getUserRepository()->getUserByEmail($this->request->mixed("email"));
        if (is_object($user)) {

            try {

                if (!$user->isConfirmed()) {
                    Message::warning(
                        $this->lpm->translate("message", "account_verification_required"),
                        RestCode::ACCOUNT_VERIFICATION_REQUIRED);
                }
                $payload = PayloadBuilder::instance()
                    ->add("iat", time())
                    ->add("nbf", time())
                    ->add("exp", time() + 1 * 60 * 60)
                    ->add("iss", "My App")
                    ->add("aud", "Your application")
                    ->add("login", $user->getEmail())
                    ->add("id", $user->getId())
                    ->toJson();
                $token = $this->makeJwt(env("SECRET_KEY"), $payload);
                Message::success(
                    $this->translate("message", "successful_authorization"),
                    ["access_token" => $token]
                );


            } catch (Exception $e) {
                throw new ExceptionApi("error", $e->getMessage(), "error", -1, $e->getPrevious());
            }
        } else {
            Message::error(
                $this->lpm->translate("message", "err_user_authorization_failed"),
                RestCode::ERR_USER_AUTHORIZATION_FAILED
            );
        }
    }
}
