<?php

namespace App\Model\Video;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="video")
 */
class Video
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="bigint")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     * @var string
     * @ORM\Column(name="alt", type="string", length=100)
     */
    private $alt;



    /**
     * Video constructor.
     * @param string $path
     * @param string $alt
     */
    public function __construct(string $path = "", string $alt = "")
    {
        $this->path = $path;
        $this->alt = $alt;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getAlt(): string
    {
        return $this->alt;
    }



    /**
     * @param string $alt
     */
    public function setAlt(string $alt): void
    {
        $this->alt = $alt;
    }



    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }



    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}
