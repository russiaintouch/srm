<?php

namespace App\Model\Role;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="RoleRepository")
 * @ORM\Table(name="role")
 */
class Role
{


    use \Engine\Core\Passport\Entity\Role\Role;
}
