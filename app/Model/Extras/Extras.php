<?php

namespace App\Model\Extras;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="ExtrasRepository")
 * @ORM\Table(name="extras")
 */
class Extras
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(name="price", type="decimal", scale=2)
     */
    private $price = 0;

    /**
     * @ORM\Column(name="limitation", type="integer")
     */
    private $limitation = 0;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="image", type="string", length=255, options={"default": null})
     */
    private $image = null;



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param mixed $name
     * @return Extras
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }



    /**
     * @param mixed $price
     * @return Extras
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * @param mixed $description
     * @return Extras
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }



    /**
     * @param mixed $image
     * @return Extras
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getLimitation()
    {
        return $this->limitation;
    }



    /**
     * @param mixed $limitation
     */
    public function setLimitation($limitation): void
    {
        $this->limitation = $limitation;
    }


}
