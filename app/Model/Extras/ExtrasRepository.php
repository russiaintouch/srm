<?php

namespace App\Model\Extras;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;



/**
 * Class ExtrasRepository
 * @package App\Model\Extras
 */
class ExtrasRepository extends EntityRepository
{


    /**
     * ExtrasRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Extras::class));
    }



    /**
     * @param int $count
     * @param int $offset
     * @return mixed
     */
    public function get(int $count = 30, int $offset = 0)
    {
        return $this->createQueryBuilder("ext")
            ->select()
            ->setMaxResults($count)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }



    /**
     * @param int $id
     * @return Extras|object|null
     */
    public function getById(int $id)
    {
        return $this->findOneBy(["id" => $id]);
    }



    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getCount()
    {
        return $this->createQueryBuilder("ext")
            ->select("count(ext.id)")
            ->getQuery()
            ->getSingleScalarResult();
    }

}
