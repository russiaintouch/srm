<?php

namespace App\Model\Package;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;



/**
 * Class PackageRepository
 * @package App\Model\Package
 */
class PackageRepository extends EntityRepository
{

    /**
     * ProductRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Package::class));
    }
}
