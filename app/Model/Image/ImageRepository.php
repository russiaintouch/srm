<?php

namespace App\Model\Image;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;



/**
 * Class ImageRepository
 * @package App\Model\Image
 */
class ImageRepository extends EntityRepository
{


    /**
     * ImageRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Image::class));
    }

}
