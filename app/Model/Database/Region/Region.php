<?php

namespace App\Model\Database\Region;

use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="RegionRepository")
 * @ORM\Table(name="region")
 */
class Region
{


    /**
     * Key
     * =======================================================
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Name
     * =======================================================
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    private $name;

    /**
     * Country
     * =======================================================
     * @var string
     * @ORM\ManyToOne(targetEntity="App\Model\Database\City\City", inversedBy="region")
     *
     */
    private $country;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }



    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }



    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }



    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }


}
