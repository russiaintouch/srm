<?php

namespace App\Model\Database\Street;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="street")
 */
class Street
{


    /**
     * Key
     * =======================================================
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Name
     * =======================================================
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    private $name;
}
