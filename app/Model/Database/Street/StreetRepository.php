<?php

namespace App\Model\Database\Street;



use Doctrine\ORM\EntityManagerInterface;
use Engine\Core\ORM\EntityRepository;



/**
 * Class StreetRepository
 * @package App\Model\Database\Street
 */
class StreetRepository extends EntityRepository
{


    /**
     * StreetRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Street::class));
    }
}
