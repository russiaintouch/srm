<?php

namespace App\Model\Database\Country;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="CountryRepository")
 * @ORM\Table(name="country")
 */
class Country
{


    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    private $name;



    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}
