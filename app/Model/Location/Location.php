<?php

namespace App\Model\Location;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="location")
 */
class Location
{


    /**
     * Key
     * =======================================================
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;


    /**
     * Latitude
     * =======================================================
     * @var string
     * @ORM\Column(name="latitude", type="string")
     */
    private $latitude;

    /**
     * Longitude
     * =======================================================
     * @var string
     * @ORM\Column(name="longitude", type="string")
     */
    private $longitude;

    /**
     * Tourism destinations
     * =======================================================
     * @var string
     * @ORM\Column(name="tourismDestinations", type="string")
     */
    private $tourismDestinations;

    /**
     * Address
     * =======================================================
     * @var string
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * Postcode/ZIP
     * =======================================================
     * @var string
     * @ORM\Column(name="postcode", type="string", length=100)
     */
    private $postcode;

    /**
     * City
     * =======================================================
     * @ORM\ManyToOne(targetEntity="App\Model\Database\City\City", inversedBy="id")
     */
    private $city;

    /**
     * Country
     * =======================================================
     * @ORM\ManyToOne(targetEntity="App\Model\Database\Country\Country", inversedBy="id", fetch="LAZY")
     */
    private $country;


    /**
     * City
     * =======================================================
     * @ORM\ManyToOne(targetEntity="App\Model\Database\Region\Region", inversedBy="id", fetch="LAZY")
     */
    private $region;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }



    /**
     * @param mixed $region
     */
    public function setRegion($region): void
    {
        $this->region = $region;
    }



    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }



    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }



    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }



    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }



    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }



    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }



    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }



    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }



    /**
     * @return string
     */
    public function getTourismDestinations(): string
    {
        return $this->tourismDestinations;
    }



    /**
     * @param string $tourismDestinations
     */
    public function setTourismDestinations(string $tourismDestinations): void
    {
        $this->tourismDestinations = $tourismDestinations;
    }



    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }



    /**
     * @param string $longitude
     */
    public function setLongitude(string $longitude): void
    {
        $this->longitude = $longitude;
    }



    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }



    /**
     * @param string $latitude
     */
    public function setLatitude(string $latitude): void
    {
        $this->latitude = $latitude;
    }


}
