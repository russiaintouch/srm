<?php

namespace App\Model\Discount;



use DateTime;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="DiscountRepository")
 * @ORM\Table(name="discount")
 */
class Discount
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="code", type="string", length=100, unique=true)
     */
    private $code;

    /**
     * Applied once
     * @ORM\Column(type="smallint")
     */
    private $AppliedOncePer = false;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $validityFrom;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $validityTo;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $limiting = 0;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $redemption = 0;

    /**
     * @var boolean
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $status = true;


    /**
     * @ORM\Column(type="decimal", scale=2, options={"default": 0})
     */
    private $minAmount = 0;


    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $taxesFees;



    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }



    /**
     * @param mixed $code
     * @return Discount
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }



    /**
     * @return DateTime|int
     */
    public function getValidityFrom()
    {
        return $this->validityFrom->getTimestamp();
    }



    /**
     * @param DateTime $validityFrom
     * @return Discount
     */
    public function setValidityFrom(DateTime $validityFrom): Discount
    {
        $this->validityFrom = $validityFrom;
        return $this;
    }



    /**
     * @return int
     */
    public function getValidityTo(): int
    {
        return $this->validityTo->getTimestamp();
    }



    /**
     * @param DateTime $validityTo
     * @return Discount
     */
    public function setValidityTo(DateTime $validityTo): Discount
    {
        $this->validityTo = $validityTo;
        return $this;
    }



    /**
     * @return string|null
     */
    public function getInternalNotes(): ?string
    {
        return $this->internalNotes;
    }



    /**
     * @param string|null $internalNotes
     * @return Discount
     */
    public function setInternalNotes(?string $internalNotes): Discount
    {
        $this->internalNotes = $internalNotes;
        return $this;
    }

    /**
     * @var null|string
     * @ORM\Column(type="text", length=1000)
     */
    private $internalNotes;



    public function __construct()
    {
        $this->validityFrom = new DateTime();
        $this->validityTo = new DateTime();
    }



    /**
     * @return mixed
     */
    public function getMinAmount()
    {
        return $this->minAmount;
    }



    /**
     * @param mixed $minAmount
     */
    public function setMinAmount($minAmount): void
    {
        $this->minAmount = $minAmount;
    }



    /**
     * @return mixed
     */
    public function getApplicability()
    {
        return $this->AppliedOncePer;
    }



    /**
     * @param $AppliedOncePer
     */
    public function setApplicability($AppliedOncePer): void
    {
        $this->AppliedOncePer = $AppliedOncePer;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return bool
     */
    public function isTaxesFees(): bool
    {
        return $this->taxesFees;
    }



    /**
     * @param bool $taxesFees
     */
    public function setTaxesFees(bool $taxesFees): void
    {
        $this->taxesFees = $taxesFees;
    }



    /**
     * @return mixed
     */
    public function getLimiting()
    {
        return $this->limiting;
    }



    /**
     * @param mixed $limiting
     */
    public function setLimiting($limiting): void
    {
        $this->limiting = $limiting;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }



    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }



    /**
     * @return mixed
     */
    public function getRedemption()
    {
        return $this->redemption;
    }



    /**
     * @param mixed $redemption
     */
    public function setRedemption($redemption): void
    {
        $this->redemption = $redemption;
    }


}
