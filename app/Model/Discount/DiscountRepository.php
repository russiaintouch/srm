<?php

namespace App\Model\Discount;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;



class DiscountRepository extends EntityRepository
{


    /**
     * DiscountRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Discount::class));
    }



    /**
     * @param int $count
     * @param int $offset
     * @return QueryBuilder
     */
    public function get($count = 10, $offset = 0)
    {
        return $this->createQueryBuilder("discount")
            ->select()
            ->setMaxResults($count)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }



    /**
     * @param int $id
     * @return mixed
     */
    public function getById(int $id)
    {
        return $this->findOneBy([
            "id" => $id
        ]);
    }



    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->createQueryBuilder("discount")
            ->delete()
            ->where("discount.id = :id")
            ->setParameter("id", $id)
            ->getQuery()
            ->execute();
    }
}
