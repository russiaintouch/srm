<?php

namespace App\Model\User;



use App\Model\Role\Role;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;



/**
 * Class UserRepository
 * @package App\Model\User
 */
class UserRepository extends EntityRepository
{


    /**
     * UserRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(User::class));
    }



    /**
     * @param int $id
     * @return object|null
     */
    public function getUserById(int $id)
    {
        return $this->findOneBy([
            "id" => $id
        ]);
    }



    /**
     * @param string $email
     * @return object|null
     */
    public function getUserByEmail(string $email): ?User
    {
        return $this->findOneBy([
            "email" => $email
        ]);
    }



    /**
     * @param string $email
     * @return bool
     */
    public function UserExists(string $email)
    {
        return is_object($this->findOneBy(["email" => $email])) ? true : false;
    }



    /**
     * @param string $email
     * @return Role
     */
    public function getRoleByEmail(string $email)
    {
        return $this->getUserByEmail($email)->getGroup()->getRole();
    }



    /**
     * @param string $email
     * @return string
     */
    public function getPermissionByEmail(string $email)
    {
        return $this->getRoleByEmail($email)->getPermission();
    }

}
