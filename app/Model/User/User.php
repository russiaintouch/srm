<?php

namespace App\Model\User;



use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;



/**
 * Class User
 * @ORM\Entity(repositoryClass="App\Model\User\UserRepository")
 * @ORM\Table(name="user")
 */
class User
{


    Use \Engine\Core\Passport\Entity\User\User;


    /**
     * @var string
     * @ORM\Column(type="string", length=255, options={"default": null})
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, options={"default": null})
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=15, unique=true)
     */
    private $phone;


    /**
     * floor. Possible values:
     * ==========================
     * 1 - female;
     * 2 - male;
     * 0 - gender is not specified.
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $sex = 0;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, options={"default": null})
     */
    private $image = '';


    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $confirmed = false;



    /**
     * User constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->createAt = new DateTime();
    }



    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }



    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName(string $firstName): User
    {
        $this->firstName = $firstName;
        return $this;
    }



    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }



    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName): User
    {
        $this->lastName = $lastName;
        return $this;
    }



    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }



    /**
     * @param string $phone
     * @return User
     */
    public function setPhone(string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }



    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }



    /**
     * @param string $image
     * @return User
     */
    public function setImage($image): User
    {
        $this->image = $image;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }



    /**
     * @param mixed $createAt
     * @return User
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
        return $this;
    }



    /**
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->confirmed;
    }



    /**
     * @param bool $confirmed
     */
    public function setConfirmed(bool $confirmed): void
    {
        $this->confirmed = $confirmed;
    }



    /**
     * пол. Возможные значения:
     * ==========================
     * 1 — женский;
     * 2 — мужской;
     * 0 — пол не указан.
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }



    /**
     * пол. Возможные значения:
     * ==========================
     * 1 — женский;
     * 2 — мужской;
     * 0 — пол не указан.
     * @param mixed $sex
     */
    public function setSex($sex): void
    {
        $this->sex = $sex;
    }


}
