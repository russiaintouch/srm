<?php

namespace App\Model\ProductType;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;



/**
 * Class ProductTypeRepository
 * @package App\Model\ProductType
 */
class ProductTypeRepository extends EntityRepository
{


    /**
     * ProductTypeRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(ProductType::class));
    }



    /**
     * @param int $count
     * @param int $offset
     * @return mixed
     */
    public function getItems($count = 30, $offset = 0)
    {
        return $this->createQueryBuilder("pt")
            ->select()
            ->setMaxResults($count)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }



    /**
     * @param int $id
     * @return ProductType|object|null
     */
    public function getById(int $id)
    {
        return $this->findOneBy(["id" => $id]);
    }




    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getCount()
    {
        return $this->createQueryBuilder("pt")
            ->select("count(pt.id)")
            ->getQuery()
            ->getSingleScalarResult();
    }
}
