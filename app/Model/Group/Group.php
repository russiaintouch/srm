<?php

namespace App\Model\Group;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="GroupRepository")
 * @ORM\Table(name="`group`")
 */
class Group
{


    Use \Engine\Core\Passport\Entity\Group\Group;

}
