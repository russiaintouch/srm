<?php

namespace App\Model\Product;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Engine\Core\ORM\EntityRepository;



/**
 * Class ProductRepository
 * @package App\Model\Product
 */
class ProductRepository extends EntityRepository
{


    /**
     * ProductRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Product::class));
    }



    /**
     * @param int $count
     * @param int $offset
     * @return mixed
     */
    public function get(int $count = 30, int $offset = 0)
    {
        return $this->createQueryBuilder("prod")
            ->select()
            ->setMaxResults($count)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }



    /**
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getCount()
    {
        return $this->createQueryBuilder("prod")
            ->select("count(prod.id)")
            ->getQuery()
            ->getSingleScalarResult();
    }
}
