<?php

namespace App\Model\Product;



use Doctrine\ORM\Mapping as ORM;



/**
 * @todo Требует детальной проработки
 *
 * @ORM\Entity
 * @ORM\Table(name="product_scheduling")
 */
class ProductScheduling
{


    /**
     * Key
     * =======================================================
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * Dates available
     * =======================================================
     * <strong>Fixed dates & times
     * <p class="red">Allows you to select dates and times when this product is available.
     * Gives you the flexibility to change the price and availability for each date/time, and to blackout dates.
     * You will need to create sessions in your calendar to open availability.</p><br>
     *
     *
     * <strong>Any date
     * <p>This means that a date is required, but the customer can choose any they wish.
     * You cannot limit availability, blackout dates or vary your prices per period if you select this option.</p><br>
     *
     * <strong>Date not required
     * <p>For example if you sell open tickets that do not require a specific date.</p><br>
     * ------------------------------------------------------
     * Example:
     * <code>
     * {"value": 1,"type": "hours"}
     *
     * @ORM\Column(type="string", length=255)
     */
    private $datesAvailable;

    /**
     * @ORM\Column(name="timesAvailable", type="json")
     */
    private $timesAvailable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $confirmBookings;


    /**
     * @ORM\Column(name="minimumNotice", type="json")
     */
    private $minimumNotice;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getDatesAvailable()
    {
        return $this->datesAvailable;
    }



    /**
     * @param mixed $datesAvailable
     */
    public function setDatesAvailable($datesAvailable): void
    {
        $this->datesAvailable = $datesAvailable;
    }



    /**
     * @return mixed
     */
    public function getConfirmBookings()
    {
        return $this->confirmBookings;
    }



    /**
     * @param mixed $confirmBookings
     */
    public function setConfirmBookings($confirmBookings): void
    {
        $this->confirmBookings = $confirmBookings;
    }



    /**
     * @return mixed
     */
    public function getMinimumNotice()
    {
        return $this->minimumNotice;
    }



    /**
     * @param mixed $minimumNotice
     */
    public function setMinimumNotice($minimumNotice): void
    {
        $this->minimumNotice = $minimumNotice;
    }



    /**
     * @return mixed
     */
    public function getTimesAvailable()
    {
        return $this->timesAvailable;
    }



    /**
     * @param mixed $timesAvailable
     */
    public function setTimesAvailable($timesAvailable): void
    {
        $this->timesAvailable = $timesAvailable;
    }

}
