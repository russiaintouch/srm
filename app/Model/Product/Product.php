<?php

namespace App\Model\Product;



use App\Model\Category\Category;
use App\Model\Extras\Extras;
use App\Model\Image\Image;
use App\Model\ProductType\ProductType;
use App\Model\Video\Video;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{


    /**
     * Key
     * =======================================================
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * Name
     * =======================================================
     * @var string
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * Unique Identifier
     * =======================================================
     * @var string
     * @ORM\Column(name="uniqueCode", type="string", length=100, options={"default": null})
     */
    private $uniqueCode = null;

    /**
     * @ORM\Column(name="productPricing", type="json")
     */
    private $productPricing;


    /**
     * Advertised price
     * =======================================================
     * @var double
     * @ORM\Column(name="advertisedPrice", type="decimal", length=5, scale=2)
     */
    private $advertisedPrice = 0;

    /**
     * Quantity
     * Min count
     * =======================================================
     * @var integer
     * @ORM\Column(name="countMin", type="integer")
     */
    private $countMin = 1;

    /**
     * Quantity
     * Max count
     * =======================================================
     * @var integer
     * @ORM\Column(name="countMax", type="integer")
     */
    private $countMax = 1;

    /**
     * Quantity label
     * =======================================================
     * Example
     * -------------------------------------------------------
     * - Singular - Passenger
     * - Plural   - Passengers
     * @ORM\Column(name="quantityLabel", type="json")
     */
    private $quantityLabel;

    /**
     * Short description
     * =======================================================
     * @var string
     * @ORM\Column(name="shortDescription", type="text", length=500)
     */
    private $shortDescription;

    /**
     * Long description
     * =======================================================
     * @var string
     * @ORM\Column(name="longDescription", type="text", options={"default": null})
     */
    private $longDescription;

    /**
     * @ORM\ManyToMany(targetEntity="App\Model\Image\Image", inversedBy="id", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="product_image", joinColumns={
     *          @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="CASCADE")}
     *
     * )
     */
    private $images;

    /**
     * Video
     * =======================================================
     * @ORM\ManyToMany(targetEntity="App\Model\Video\Video", inversedBy="id", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="product_video", joinColumns={
     *          @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="video_id", referencedColumnName="id", onDelete="CASCADE")}
     *
     * )
     */
    private $videos;

    /**
     *
     * @ORM\Column(name="options", type="json")
     */
    private $options;

    /**
     * Extras
     * =======================================================
     * @ORM\ManyToMany(targetEntity="App\Model\Extras\Extras", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="product_extras", joinColumns={
     *          @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="extras_id", referencedColumnName="id", onDelete="CASCADE")}
     *
     * )
     */
    private $extras;

    /**
     * @var ProductType
     * @ORM\ManyToOne(targetEntity="App\Model\ProductType\ProductType", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $productType;

    /**
     * Terms of Use
     * =======================================================
     * @var string
     * @ORM\Column(name="termsUse", type="text", length=3000)
     */
    private $termsUse;

    /**
     * @var ProductScheduling
     * @ORM\ManyToOne(targetEntity="ProductScheduling", inversedBy="id", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $scheduling;

    /**
     * Category
     * =======================================================
     * @ORM\ManyToMany(targetEntity="App\Model\Category\Category", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="product_category", joinColumns={
     *          @ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *
     * )
     */
    private $category;



    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->extras = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->videos = new ArrayCollection();
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }



    /**
     * @return mixed
     */
    public function getCountMin()
    {
        return $this->countMin;
    }



    /**
     * @param mixed $countMin
     */
    public function setCountMin($countMin): void
    {
        $this->countMin = $countMin;
    }



    /**
     * @return mixed
     */
    public function getLongDescription()
    {
        return $this->longDescription;
    }



    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }



    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }



    /**
     * @return mixed
     */
    public function getCountMax()
    {
        return $this->countMax;
    }



    /**
     * @param mixed $countMax
     */
    public function setCountMax($countMax): void
    {
        $this->countMax = $countMax;
    }



    /**
     * @return ArrayCollection
     */
    public function getExtras()
    {
        return $this->extras;
    }



    /**
     * @param Extras $extras
     */
    public function addExtras($extras = null): void
    {
        $this->extras->add($extras);
    }



    /**
     * @return ProductType
     */
    public function getProductType(): ProductType
    {
        return $this->productType;
    }



    /**
     * @param ProductType $productType
     */
    public function setProductType(ProductType $productType): void
    {
        $this->productType = $productType;
    }



    /**
     * @param mixed $longDescription
     */
    public function setLongDescription($longDescription): void
    {
        $this->longDescription = $longDescription;
    }



    /**
     * @param mixed $extras
     */
    public function setExtras(?ArrayCollection $extras): void
    {
        $this->extras = $extras;
    }



    /**
     * @return string
     */
    public function getUniqueCode(): string
    {
        return $this->uniqueCode;
    }



    /**
     * @param string $uniqueCode
     */
    public function setUniqueCode(string $uniqueCode): void
    {
        $this->uniqueCode = $uniqueCode;
    }



    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }




    public function addImage(Image $image)
    {
        return $this->images->add($image);
    }



    /**
     * @param string $path
     * @param string $alt
     */
    public function AddVideo(string $path, string $alt)
    {
        $this->videos->add(new Video($path, $alt));
    }



    public function getVideos()
    {
        return $this->videos;
    }



    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }



    /**
     * @param mixed $options
     */
    public function setOptions(string $options): void
    {
        $this->options = json_decode($options);
    }



    /**
     * @return string
     */
    public function getTermsUse(): string
    {
        return $this->termsUse;
    }



    /**
     * @param string $termsUse
     */
    public function setTermsUse(string $termsUse): void
    {
        $this->termsUse = $termsUse;
    }



    /**
     * @return float
     */
    public function getAdvertisedPrice(): float
    {
        return $this->advertisedPrice;
    }



    /**
     * @param float $advertisedPrice
     */
    public function setAdvertisedPrice( float $advertisedPrice): void
    {
        $this->advertisedPrice = $advertisedPrice;
    }



    /**
     * @return mixed
     */
    public function getProductPricing()
    {
        return $this->productPricing;
    }



    /**
     * @param mixed $productPricing
     */
    public function setProductPricing($productPricing): void
    {
        $this->productPricing = json_decode($productPricing);
    }



    /**
     * @return mixed
     */
    public function getQuantityLabel()
    {
        return $this->quantityLabel;
    }



    /**
     * @param mixed $quantityLabel
     */
    public function setQuantityLabel($quantityLabel): void
    {
        $this->quantityLabel = json_decode($quantityLabel);
    }



    /**
     * @return ProductScheduling
     */
    public function getScheduling(): ?ProductScheduling
    {
        return $this->scheduling;
    }



    /**
     * @param ProductScheduling $scheduling
     */
    public function setScheduling(?ProductScheduling $scheduling): void
    {
        $this->scheduling = $scheduling;
    }



    /**
     * @return Category
     */
    public function getCategory()
    {

        return $this->category;
    }



    /**
     * @param Category $category
     */
    public function setCategory(?Category $category): void
    {
        $this->category = $category;
    }


}
