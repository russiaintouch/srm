<?php

namespace App\Model\Permission;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="permission")
 */
class Permission
{


    Use \Engine\Core\Passport\Entity\Permission\Permission;

}
