<?php

namespace App\Model\Category;



use Doctrine\ORM\EntityManagerInterface;
use Engine\Core\ORM\EntityRepository;



/**
 * Class CategoryRepository
 * @package App\Model\Category
 */
class CategoryRepository extends EntityRepository
{

    /**
     * ExtrasRepository constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getMetadataFactory()->getMetadataFor(Category::class));
    }
}
