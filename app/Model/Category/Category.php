<?php

namespace App\Model\Category;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass="CategoryRepository")
 * @ORM\Table(name="category")
 */
class Category
{


    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="text", length=1000)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(name="relCanonicalUrl", type="string")
     */
    private $relCanonicalURL = "";

    /**
     * @var string
     * @ORM\Column(name="SEODescription", type="string",  length=100)
     */
    private $SEODescription = "";

    /**
     * @var boolean
     * @ORM\Column(name="indexing", type="boolean")
     */
    private $indexing = false;

    /**
     * @var boolean
     * @ORM\Column(name="visibility", type="boolean")
     */
    private $visibility = false;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param mixed $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * @param mixed $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }



    /**
     * @return string
     */
    public function getRelCanonicalURL(): string
    {
        return $this->relCanonicalURL;
    }



    /**
     * @param string $relCanonicalURL
     * @return Category
     */
    public function setRelCanonicalURL(string $relCanonicalURL): Category
    {
        $this->relCanonicalURL = $relCanonicalURL;
        return $this;
    }



    /**
     * @return string
     */
    public function getSEODescription(): string
    {
        return $this->SEODescription;
    }



    /**
     * @param string $SEODescription
     * @return Category
     */
    public function setSEODescription(string $SEODescription): Category
    {
        $this->SEODescription = $SEODescription;
        return $this;
    }



    /**
     * @return bool
     */
    public function isIndexing(): bool
    {
        return $this->indexing;
    }



    /**
     * @param bool $indexing
     * @return Category
     */
    public function setIndexing(bool $indexing): Category
    {
        $this->indexing = $indexing;
        return $this;
    }



    /**
     * @return bool
     */
    public function isVisibility(): bool
    {
        return $this->visibility;
    }



    /**
     * @param bool $visibility
     * @return Category
     */
    public function setVisibility(bool $visibility): Category
    {
        $this->visibility = $visibility;
        return $this;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}
