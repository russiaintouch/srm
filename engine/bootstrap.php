<?php
require __DIR__ . '/../vendor/autoload.php';

use Engine\Application;
use Engine\DI\DI;



try {
    $di = new DI();
    initServiceProvider($di, __DIR__ . '/Config/Service.php');
    if (ENV != 'Console') {
        $cms = new Application($di);
        $cms->run();
    }

} catch (Exception $e) {
    echo $e->getMessage();
}

