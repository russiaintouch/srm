<?php


namespace Engine;


use DateTime;
use Doctrine\Common\Annotations\Annotation\IgnoreAnnotation;
use Doctrine\ORM\Mapping as ORM;
use Exception;



/**
 * Class BaseModel
 * @package Api
 * @IgnoreAnnotation("BaseModel")
 */
abstract class Model
{
    /**
     * BaseModel constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->setUpdateAt(new DateTime("now"));
        $this->setCreateAt(new DateTime("now"));
    }



    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $createAt;
    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $updateAt;



    /**
     * @return DateTime
     */
    public function getUpdateAt():DateTime
    {
        return $this->updateAt;
    }



    /**
     * @param DateTime $updateAt
     */
    public function setUpdateAt(DateTime $updateAt):void
    {
        $this->updateAt = $updateAt;
    }



    /**
     * @return DateTime
     */
    public function getCreateAt():DateTime
    {
        return $this->createAt;
    }



    /**
     * @param DateTime $createAt
     */
    public function setCreateAt(DateTime $createAt):void
    {
        $this->createAt = $createAt;
    }
}
