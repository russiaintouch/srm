<?php

namespace Engine;



use Engine\Core\LPManager\LPManager;
use Engine\Core\Request\Request;
use Engine\Core\Response\Response;
use Engine\Core\Router\DispatchedRoute;
use Engine\DI\DI;
use Engine\Helper\Common;
use Exception;
use Monolog\Logger;
use Symfony\Component\Serializer\Serializer;



/**
 * Class Application
 * @package Engine
 */
class Application
{


    /**
     * @var Core\Router\Router
     */
    public $router;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var DI
     */
    private $di;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var LPManager
     */
    private $lpm;



    /**
     * Cms constructor.
     *
     * @param $di
     *
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->router = $this->di->get("router");
        $this->logger = $this->di->get("logger");
        $this->request = $this->di->get("request");
        $this->serializer = $this->di->get("serializer");
        $this->response = $this->di->get("response");
        $this->lpm = $this->di->get("lpm");
    }



    /**
     * Run cms
     */
    public function run()
    {
        try {
            $this->loadRouters();
            $routerDispatch = $this->router->dispatch(Common::getMethod(), Common::getPathUrl());
            if ($routerDispatch == null) {
                $routerDispatch = new DispatchedRoute("ErrorController:error");
            }
            list($class, $action) = explode(":", $routerDispatch->getController(), 2);
            $controller = $this->makeControllerNamespace($class, ENV);
            $parameters = $routerDispatch->getParameters();
            $call_res = $this->call($controller, $action, $parameters);
            if ($call_res === false) {
                throw new Exception("Ошибка вызова функции \"call_user_func_array\"");
            }

        } catch (Exception $e) {

            $data = [
                "code" => UNKNOWN_ERROR,
                "level" => MSG_LEVEL_ERROR,
                "title" => $this->lpm->translate("message", "error"),
                "msg" => $this->lpm->translate("message", "unknown_error"),
                "debug" => $e->getMessage(),
            ];
            $json = $this->serializer->serialize($data, "json");
            $this->response->jsonSend($json);
            $this->logger->critical($e->getMessage(), $e->getTrace());
        }
    }



    /**
     * @param string $controller
     * @param $action
     * @param $parameters
     * @return mixed
     * @throws Exception
     */
    private function call(string $controller, string $action, $parameters)
    {
        if (class_exists($controller)) {
            return call_user_func_array([new $controller($this->di), $action], $parameters);
        } else {
            throw new Exception("Class [$controller] not exists!");
        }
    }



    /**
     * @param string $class
     * @param string $env
     * @return string
     */
    private function makeControllerNamespace(string $class, string $env)
    {
        return "\\App\\Http\\Controller\\" . ucfirst(strtolower($env)) . "\\$class";
    }



    private function loadRouters()
    {
        /** @noinspection PhpIncludeInspection */
        require_once $this->getRoutersPath();
    }



    private function getRoutersPath()
    {
        return ROOT_DIR . "/app/Routers/" . mb_strtolower(ENV) . "/route.php";
    }
}
