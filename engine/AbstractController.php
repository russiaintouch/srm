<?php

namespace Engine;



use Doctrine\ORM\EntityManager;
use Engine\Core\Config\Config;
use Engine\Core\Database\SafeMySQL;
use Engine\Core\DataRepository\Data;
use Engine\Core\LPManager\LPManager;
use Engine\Core\Middleware\Middleware;
use Engine\Core\Request\Request;
use Engine\Core\Response\Response;
use Engine\Core\Session\Session;
use Engine\Core\Setting\Setting;
use Engine\DI\DI;
use Exception;
use GuzzleHttp\Client;
use Monolog\Logger;
use Rakit\Validation\Validator;



/**
 * Class Controller
 * @package Engine
 */
abstract class AbstractController
{

     /**
     * @var DI
     */
    protected $di;

    /**
     * The middleware registered on the controller.
     *
     * @var Middleware
     */
    protected $middleware;

    /**
     * @var LPManager
     */
    protected $lpm;

    /**
     * @var Data
     */
    protected $data;

    /**
     * Language localization
     * @var string
     */
    protected $lang;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Config
     */
    protected $config;
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var \Engine\Core\Plugin\Plugin
     */
    protected $plugin;

    /**
     * @var Setting
     */
    protected $setting;
    /**
     * @var SafeMySQL
     */
    protected $db;
    /**
     * @var Client
     */
    protected $http;

    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var Response
     */
    protected $response;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Validator
     */
    protected $validator;



    /**
     * Controller constructor.
     * @param DI $di
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        $this->lang = "ru";

        $this->di = $di;
        $this->config = $this->di->get("config");
        $this->setting = $this->di->get("setting");
        $this->request = $this->di->get("request");
        $this->em = $this->di->get("em");
        $this->http = $this->di->get("http");
        $this->logger = $this->di->get("logger");
        $this->lpm = $this->di->get("lpm");
        $this->response = $this->di->get("response");
        $this->session = $this->di->get("session");
        $this->middleware = $this->di->get("middleware");
        $this->validator = $this->di->get("validator");
    }





    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->di->get($key);
    }



    /**
     * @return AbstractController
     */
    public function initVars()
    {
        $vars = array_keys(get_object_vars($this));
        foreach ($vars as $var) {
            if ($this->di->has($var)) {
                $this->{$var} = $this->di->get($var);
            }
        }
        return $this;
    }



    /**
     * @return Middleware
     */
    public function getMiddleware(): Middleware
    {
        return $this->middleware;
    }



    /**
     * @return Validator
     * @see https://github.com/rakit/validation
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

}
