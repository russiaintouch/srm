<?php

/**
 * Файл конфигурации (php.ini) считывается при запуске PHP. Для версий серверных модулей PHP
 * это происходит только один раз при запуске веб-сервера. Для CGI и CLI версий это происходит
 * при каждом вызове.
 *
 */

$ini_items = [
    "session.use_strict_mode" => 1,
    "display_errors" => "On",
];


foreach ($ini_items as $varname => $newvalue){
    ini_set($varname, $newvalue);
}


