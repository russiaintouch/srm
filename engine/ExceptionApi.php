<?php


namespace Engine;

use Exception;
use Throwable;

/**
 * Class ExceptionApi
 * @package Engine
 */
class ExceptionApi extends Exception
{
    /**
     * @var string
     */
    private $title;



    /**
     * @return string
     */
    public function getTitle():string
    {
        return $this->title;
    }



    /**
     * @return string
     */
    public function getLevel():string
    {
        return $this->level;
    }
    /**
     * @var string
     */
    private $level = 'error';



    /**
     * ExceptionApi constructor.
     *
     * @param string         $title
     * @param string         $msg
     * @param string         $level
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $title, string $msg = "", $level = 'error', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($msg, $code, $previous);
        $this->title = $title;
        $this->level = $level;
    }



    /**
     * @return array
     */
    public function getArray()
    {
        return [
            'code' => $this->getCode(),
            'title' => $this->getTitle(),
            'msg' => $this->getMessage(),
            'level' => $this->getLevel()
        ];
    }
}
