<?php

define('DS', DIRECTORY_SEPARATOR);
define('DOCUMENT_ROOT', dirname($_SERVER["DOCUMENT_ROOT"], 1) . DS . $_SERVER['HTTP_HOST']);
define('ROOT_DIR', dirname(__DIR__, 1));
define('REMOTE_ADDRESS', $_SERVER["REMOTE_ADDR"]);
define('REQUEST_METHOD', $_SERVER["REQUEST_METHOD"]);


/**
 * Системные коды ошибок
 */


/** Языковый пакет не найден */
const LANGUAGE_PACKAGE_NOT_FOUND = 1001;
/** Языковый пакет поврежден */
const LANGUAGE_PACKAGE_DAMAGED = 1002;
/** Передан неизвестный метод. */
const UNKNOWN_METHOD = 3;




/**
 * Пользовательские коды ошибок
 */

class RestCode
{
    /**
     * Не верный маркер доступа
     */
    const ERR_INVALID_ACCESS_TOKEN = 4;

    /** Авторизация пользователя не удалась.  */
    const ERR_USER_AUTHORIZATION_FAILED = 5;

    /** Доступ запрещён.  */
    const ERR_USER_ACCESS_DENIED = 15;

    /** Авторизация временно недоступна */
    const ERR_AUTHORIZATION_TEMPORARILY_UNAVAILABLE = 16;

    /** Требуется авторизация пользователя.  */
    const USER_AUTHORIZATION_REQUIRED = 17;

    /** Пользователь уже зарегистрирован  */
    const USER_ALREADY_EXISTS = 18;

    /** Пользователь не существует */
    const USER_DOES_NOT_EXIST = 19;

    /** Требуется подтверждение аккаунта */
    const ACCOUNT_VERIFICATION_REQUIRED = 20;

    /** Некорректный номер телефона */
    const INVALID_PHONE_NUMBER = 30;

    /** Некорректные данные запроса или отсутствуют таковые */
    const ERR_INVALID_DATA = 31;

    /** Код подтверждения недействительный */
    const INVALID_CONFIRM_CODE = 32;

    /** Произошла неизвестная ошибка. */
    const ERR_UNKNOWN_ERROR = 1000;
}




const MSG_LEVEL_ERROR = 'error';
const MSG_LEVEL_WARNING = 'warning';
const MSG_LEVEL_SUCCESS = 'success';
const MSG_LEVEL_INFO = 'info';


/**
 * Регулярные выражения
 */
const PREG_IS_EMAIL = '/(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/m';


const DI_KEY_JWT_DATA = 'jwt_data';
