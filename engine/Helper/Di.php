<?php

namespace Engine\Helper;



/**
 * Class Di
 * @package Engine\Helper
 */
class Di
{


    /**
     * @return \Engine\DI\DI
     */
    public static function di()
    {
        global $di;
        return $di;
    }



    /**
     * @param string $key
     * @return bool
     */
    public static function get(string $key)
    {
        return self::di()->get($key);
    }



    /**
     * @param string $key
     * @param $value
     * @return \Engine\DI\DI
     */
    public static function set(string $key, $value)
    {
        return self::di()->set($key, $value);
    }

}
