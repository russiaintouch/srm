<?php

namespace Engine\Helper;



use Engine\Core\LPManager\LPManager;
use Engine\Core\Response\Response;
use Exception;
use RestCode;
use Symfony\Component\Serializer\Serializer;



/**
 * Class Message
 * @package Engine\Helper
 */
class Message
{


    /**
     * @param string $msg
     * @param int $code
     * @param array $data
     */
    static public function error(string $msg, int $code = -1, $data = [])
    {

        self::sendMessage(self::lpm()->translate('message', 'error'), $msg, MSG_LEVEL_ERROR, $code, $data);
    }



    /**
     * @param string $msg
     * @param int $code
     */
    static public function warning(string $msg = "", int $code = 0)
    {
        self::sendMessage(self::lpm()->translate('message', 'warning'), $msg, MSG_LEVEL_WARNING, $code);
    }



    /**
     * @param string $msg
     * @param int $code
     */
    static public function info(string $msg = "", int $code = 0)
    {
        self::sendMessage(self::lpm()->translate('message', 'info'), $msg, MSG_LEVEL_INFO, $code);
    }



    /**
     * @param string $msg
     * @param array $data
     */
    static public function success(string $msg = "", $data = [])
    {
        self::sendMessage(self::lpm()->translate('message', 'success'), $msg, MSG_LEVEL_SUCCESS, 0, $data);
    }



    /**
     * @param array $errors
     */
    static public function msgInvalidData(array $errors)
    {
        self::error(self::lpm()->translate("error", "invalid_data"), RestCode::ERR_INVALID_DATA, ["errors" => $errors]);
    }



    /**
     * @param string $package
     * @param string $key
     *
     * @return string
     * @throws Exception
     */
    static protected function translate(string $package, string $key)
    {
        return self::lpm()->translate($package, $key);
    }



    /**
     * @param string $title
     * @param string $msg
     * @param string $level
     * @param int $code
     * @param array $data
     */
    static protected function sendMessage(string $title, string $msg, string $level, int $code = 0, $data = [])
    {
        $s_data = [
            'code' => $code,
            'level' => $level,
            'title' => $title,
            'msg' => $msg
        ];
        $json = self::getSerialize()->serialize(array_merge($s_data, $data), 'json');
        self::getResponse()->jsonSend($json);
        exit;
    }



    /**
     * @return LPManager
     */
    static private function lpm(): LPManager
    {
        return Di::get('lpm');
    }



    /**
     * @return Serializer
     */
    static private function getSerialize(): Serializer
    {
        return Di::get('serializer');
    }



    /**
     * @return Response
     */
    static private function getResponse(): Response
    {
        return Di::get('response');
    }

}
