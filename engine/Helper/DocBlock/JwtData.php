<?php

namespace Engine\Helper\DocBlock;



/**
 * DocBlock
 * Class JwtData
 * @package Engine\Helper\DocBlock
 */
class JwtData
{
    public $jti;
    public $ait;
    public $nbf;
    public $exp;
    public $iss;
    public $email;
    public $id;
}
