<?php
/**
 * It is necessary to set priorities correctly when initializing modules.
 */
return [
    Engine\Service\Passport\Provider::class,
    Engine\Service\Validator\Provider::class,
    Engine\Service\Orm\Provider::class,
    Engine\Service\Setting\Provider::class,
    Engine\Service\Router\Provider::class,
    Engine\Service\Request\Provider::class,
    Engine\Service\Plugin\Provider::class,
    Engine\Service\Logger\Provider::class,
    Engine\Service\LPManager\Provider::class,
    Engine\Service\Http\Provider::class,
    Engine\Service\Serializer\Provider::class,
    Engine\Service\Response\Provider::class,
    Engine\Service\Session\Provider::class,
    Engine\Service\Middleware\Provider::class,
];
