<?php

namespace Engine;


use Engine\DI\DI;
use Monolog\Logger;



/**
 * Class Service
 * @package Engine
 */
abstract class Service
{
    /**
     * @var DI
     */
    protected $di;

    /**
     * @var AbstractModel
     */
    protected $model;
    /**
     * @var Logger
     */
    protected $logger;



    /**
     * Service constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->model = $this->di->get('model');
        $this->logger = $this->di->get('logger');
    }



    /**
     * @return DI
     */
    public function getDI()
    {
        return $this->di;
    }
}
