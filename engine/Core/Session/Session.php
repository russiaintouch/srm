<?php


namespace Engine\Core\Session;

/**
 * Class Session
 * @package Engine\Core\Session
 */
class Session
{
    /**
     * @return string
     */
    public function sid(): string
    {
        return session_id();
    }



    /**
     * @param string $prefix
     *
     * @return string
     */
    public function create_sid(string $prefix): string
    {
        return session_create_id($prefix);
    }



    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        if (isset($_SESSION[$key])){
            return true;
        }

        return false;
    }



    /**
     * @param string $key
     * @param bool   $default
     *
     * @return mixed
     */
    public function get(string $key, $default = false)
    {
        if (self::has($key)){
            return $_SESSION[$key];
        }

        return $default;
    }



    /**
     * @param string $key
     */
    public function remove(string $key)
    {
        if (self::has($key)){
            unset($_SESSION[$key]);
        }
    }



    /**
     * @param string $key
     * @param        $value
     */
    public function set(string $key, $value): void
    {
        $_SESSION[$key] = $value;
    }
}
