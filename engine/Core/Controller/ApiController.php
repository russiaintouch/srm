<?php

namespace Engine\Controller;



use App\Model\Role\Role;
use App\Model\User\UserRepository;
use Engine\AbstractController;
use Engine\Core\Passport\Passport;
use Engine\Core\Passport\Payload;
use Engine\DI\DI;
use Engine\Foundation\JWT\JWTData;
use Engine\Foundation\Permission\PermissionMethod;
use Engine\Helper\Header;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Component\Serializer\Serializer;



/**
 * Class ApiController
 * @package Engine
 */
class ApiController extends AbstractController
{


    use PermissionMethod, JWTData;

    /**
     * @var Serializer
     */
    protected $serializer;

    protected $jwtData;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /** @var Passport */
    private $passport;



    /**
     * ApiController constructor.
     *
     * @param DI $di
     *
     * @throws Exception
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);
        $this->serializer = $di->get("serializer");
        $this->passport = $di->get("passport");
    }



    /**
     * @return UserRepository|null
     */
    public function getUserRepository(): ?UserRepository
    {
        if (!is_object($this->userRepository)) $this->userRepository = new UserRepository($this->em);
        return $this->userRepository;
    }



    /**
     * @param string $email
     * @return Role|null
     */
    public function getRole(string $email): ?Role
    {
        return $this->getUserRepository()->getRoleByEmail($email);
    }



    /**
     * @return Passport
     */
    public function getPassport(): ?Passport
    {
        return $this->passport;
    }



    /**
     * @return Payload|null
     */
    protected function payload(): ?Payload
    {
        return $this->passport->payload(Header::getBearerAuthToken());
    }



    /**
     * @param string $package
     * @param string $key
     *
     * @return string
     */
    protected function translate(string $package, string $key)
    {
        return $this->lpm->translate($package, $key);
    }



    /**
     * @param string $email
     * @param string $code
     */
    protected function sendEmailConfirm(string $email, string $code)
    {
        $transport = new Swift_SmtpTransport();
        $transport->setPassword(env("SERVICE_MAILER_PASSWORD"));
        $transport->setUsername(env("SERVICE_MAILER_ADDRESS"));
        $transport->setPort(env("SERVICE_MAILER_PORT"));
        $transport->setHost(env("SERVICE_MAILER_HOST"));
        $transport->setEncryption(env("SERVICE_MAILER_ENCRYPTION"));
        $transport->setAuthMode(env("SERVICE_MAILER_AUTHMODE"));
        $mailer = new Swift_Mailer($transport);
        $message = (new Swift_Message("Код подтверждения регистрации"))
            ->setFrom("payment@cn-expert.pro")
            ->setTo($email)
            ->setBody(
                "Код подтверждения регистрации: $code",
                "text/html"
            );
        $mailer->send($message);
    }


}
