<?php

namespace Engine\Core\ORM;



use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping;



/**
 * Class EntityRepository
 * @package Engine\Core\ORM
 */
class EntityRepository extends \Doctrine\ORM\EntityRepository
{


    /**
     * EntityRepository constructor.
     * @param EntityManagerInterface $em
     * @param Mapping\ClassMetadata $class
     */
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }



    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        $this->createQueryBuilder("entity")
            ->delete()
            ->where("entity.id = :id")
            ->setParameter("id", $id)
            ->getQuery()
            ->execute();
    }



    /**
     * @param int $id
     * @return object|null
     */
    public function getById(int $id)
    {
        return $this->findOneBy(["id" => $id]);
    }



    /**
     * @param int $offset
     * @param int $count
     * @return mixed
     */
    public function get(int $offset, int $count)
    {
        return $this->createQueryBuilder("entity")
            ->select()
            ->setFirstResult($offset)
            ->setMaxResults($count)
            ->getQuery()
            ->getResult();
    }
}
