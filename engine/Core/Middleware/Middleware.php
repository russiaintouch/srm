<?php

namespace Engine\Core\Middleware;



use Engine\Core\LPManager\LPManager;
use Engine\Core\Request\Request;
use Engine\Core\Router\Router;
use Engine\Helper\Di;



/**
 * Class Middleware
 * @package Engine\Core\Middleware
 *
 */
class Middleware
{


    /** @var Di */
    private $di;

    /** @var LPManager */
    private $lpm;

    /** @var Request */
    private $request;

    /** @var Router */
    private $router;



    public function __construct()
    {
        $this->lpm = Di::get("lpm");
        $this->request = Di::get("request");
        $this->router = Di::get("router");
        $this->di = Di::di();
    }



    /**
     * @return LPManager
     */
    public function getLpm(): LPManager
    {
        return $this->lpm;
    }



    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }



    /**
     * @param MiddlewareInterface $mw
     * @return Middleware
     */
    public function execute(MiddlewareInterface $mw)
    {
        $mw->handle();
        return $this;
    }



    /**
     * @return Router
     */
    public function getRouter(): Router
    {
        return $this->router;
    }



    /**
     * @return Di
     */
    public function getDi(): Di
    {
        return $this->di;
    }
}
