<?php

namespace Engine\Core\Middleware;



use Engine\Core\Request\Request;
use Psr\Http\Message\ResponseInterface;



/**
 * Handles a server request and produces a response.
 *
 * An HTTP request handler process an HTTP request in order to produce an
 * HTTP response.
 * @see https://www.php-fig.org/psr/psr-15/#22-psrhttpservermiddlewareinterface
 */
interface RequestHandlerInterface
{


    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     * @param Request $request
     * @return ResponseInterface
     */
    public function handle(Request $request): ResponseInterface;
}
