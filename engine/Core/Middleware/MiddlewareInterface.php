<?php

namespace Engine\Core\Middleware;



/**
 * Handles a server request and produces a response.
 *
 * An HTTP request handler process an HTTP request in order to produce an
 * HTTP response.
 * @see https://www.php-fig.org/psr/psr-15/#22-psrhttpservermiddlewareinterface
 */
interface MiddlewareInterface
{


    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     * @return mixed
     */
    public function handle();

}
