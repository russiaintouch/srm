<?php

namespace Engine\Core\Passport;



use Engine\Core\Passport\Permission\Permission;



/**
 * Class Auth
 * @package Engine\Core\Auth
 */
class Passport
{


    Use JWT;


    /** @var string */
    private $key;

    /** @var Payload */
    private $payload;

    private $permission;



    /**
     * Passport constructor.
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }



    /**
     * @param string $token
     * @return bool
     */
    final public function verification(string $token)
    {
        return $this->jwsVerifier($token, $this->key);
    }



    /**
     * @param string $token
     * @return Payload
     */
    final public function payload(string $token): ?Payload
    {
        if (!isset($this->payload)){
            $this->payload = new Payload($this->getPayLoad($token, $this->key));
        }

        return $this->payload;
    }



    /**
     * @return Permission
     */
    final public function permission()
    {
        if (!isset($this->permission)){
            $this->permission = new Permission();
        }

        return $this->permission;
    }

}
