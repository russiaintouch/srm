<?php

namespace Engine\Core\Passport\Permission;


use Engine\Core\LPManager\LPManager;
use Engine\Core\Passport\Entity\Role\Role;
use Engine\Helper\Di;
use Engine\Helper\Message;



/**
 * Class Permission
 * @package Engine\Core\Passport\Permission
 */
class Permission
{



    /**
     * @param Role|null $role
     * @param $action
     * @return bool
     */
    public function checkPermission($role, $action)
    {
        $permissions_array = $role->getPermissions();
        foreach ($permissions_array as $permissions) {
            /** @var Permission $permissions */
            if ($permissions->getAction() == $action) {
                return true;
            }
        }
        Message::warning($this->lpm()->translate('action', "action_allowed_" . strtolower($action)));
    }



    private function lpm(): ?LPManager
    {
        return Di::get('lpm');
    }
}
