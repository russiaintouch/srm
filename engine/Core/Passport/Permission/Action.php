<?php

namespace Engine\Core\Passport\Permission;



/**
 * Class Action
 * @package Engine\Foundation\Permission
 */
class Action
{


    public const ADD = "ADD";
    public const GET = "GET";
    public const DELETE = "DELETE";
    public const EDIT = "EDIT";
    public const VIEW = "VIEW";
    public const SET_PERMISSION = "SET_PERMISSION";
}
