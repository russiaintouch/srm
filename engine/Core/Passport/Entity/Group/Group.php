<?php

namespace Engine\Core\Passport\Entity\Group;



use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="group")
 */
trait Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Role\Role", inversedBy="group", fetch="EXTRA_LAZY")
     */
    private $role;



    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }



    /**
     * @param mixed $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}
