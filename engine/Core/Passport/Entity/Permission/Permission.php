<?php

namespace Engine\Core\Passport\Entity\Permission;



use Doctrine\ORM\Mapping as ORM;
use Engine\Foundation\Permission\Action;



/**
 * @ORM\Entity
 * @ORM\Table(name="permission")
 */
trait Permission
{


    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="action", type="string", length=30)
     */
    private $action = Action::VIEW;



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }



    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }



    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

}
