<?php

namespace Engine\Core\Passport\Entity\Role;



use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 * @ORM\Table(name="role")
 */
trait Role
{


    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Model\Permission\Permission", inversedBy="id")
     */
    private $permissions;



    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }



    /**
     * @return mixed
     */
    public function getPermissions()
    {
        return $this->permissions;
    }



    /**
     * @param mixed $permissions
     */
    public function setPermissions($permissions): void
    {
        $this->permissions = $permissions;
    }



    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
