<?php

namespace Engine\Core\Passport\Entity\User;



use Doctrine\ORM\Mapping as ORM;
use Engine\Core\Passport\Entity\Group\Group;



/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
trait User
{


    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Group\Group", inversedBy="id", fetch="EXTRA_LAZY")
     */
    private $group;

    /**
     * @var string
     * @ORM\Column(type="string",length=255)
     */
    private $password;



    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }



    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }



    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }



    /**
     * @param Group $group
     */
    public function setGroup($group): void
    {
        $this->group = $group;
    }



    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }



    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }


}
