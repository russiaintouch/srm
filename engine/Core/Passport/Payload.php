<?php

namespace Engine\Core\Passport;



/**
 * Class PayLoad
 * @method id
 * @method login
 * @method iss
 * @method exp
 * @method nbf
 * @method iat
 * @method jti
 * @package Engine\Core\Passport
 */
class Payload
{


    private $store;



    /**
     * PayLoad constructor.
     * @param string $payload
     */
    public function __construct(?string $payload)
    {
        if (is_string($payload))
            $this->store = json_decode($payload, true);
    }



    /**
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        if (is_array($this->store)) {
            return isset($this->store[$name]) ? $this->store[$name] : null;
        }
        return null;
    }


}
