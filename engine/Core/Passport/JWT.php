<?php

namespace Engine\Core\Passport;



use Exception;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\HS512;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;



/**
 * Trait JWT
 * @package Engine\Core\Passport
 */
trait JWT
{

    /**
     * @param string $token
     * @param string $key
     * @return bool
     */
    protected function jwsVerifier(string $token, string $key)
    {
        try {
            $jwsVerifier = new JWSVerifier($this->algorithmManager());
            $jwk = new JWK([
                "kty" => "oct",
                "k" => $key
            ]);
            $serializer = new CompactSerializer();
            $jws = $serializer->unserialize($token);
            $isVerified = $jwsVerifier->verifyWithKey($jws, $jwk, 0);
            if ($isVerified)
                return true;
            return false;
        } catch (Exception $e) {
            return false;
        }
    }



    /**
     * @param string $token
     * @param string $key
     * @return string|null
     */
    protected function getPayLoad(string $token, string $key)
    {
        try {
            $jwsVerifier = new JWSVerifier($this->algorithmManager());
            $jwk = new JWK([
                "kty" => "oct",
                "k" => $key
            ]);
            $serializer = new CompactSerializer();
            $jws = $serializer->unserialize($token);
            $isVerified = $jwsVerifier->verifyWithKey($jws, $jwk, 0);
            if ($isVerified)
                return $jws->getPayload();
            return null;
        } catch (Exception $e) {
            return null;
        }
    }



    /**
     * @param string $key
     * @param string $payload
     * @return string
     */
    protected function makeJwt(string $key, string $payload)
    {
        $jwsBuilder = new JWSBuilder($this->algorithmManager());
        $jwk = new JWK([
            "kty" => "oct",
            "k" => $key
        ]);
        $jws = $jwsBuilder
            ->create()// We want to create a new JWS
            ->withPayload($payload)// We set the payload
            ->addSignature($jwk, ["alg" => "HS512"])// We add a signature with a simple protected header
            ->build();
        $serializer = new CompactSerializer(); // The serializer
        return $serializer->serialize($jws, 0); //
    }



    /**
     * @return AlgorithmManager
     */
    private function algorithmManager()
    {
        return new AlgorithmManager([
            new HS512(),
        ]);
    }

}
