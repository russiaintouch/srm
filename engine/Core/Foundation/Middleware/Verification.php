<?php

namespace Engine\Foundation\Middleware;



use Engine\Core\Middleware\Middleware;
use Engine\Core\Middleware\MiddlewareInterface;
use Engine\Core\Passport\JWT;
use Engine\Helper\Header;
use Engine\Helper\Message;
use Exception;
use RestCode;



/**
 * Class Verification
 * @package App\Middleware
 */
class Verification extends Middleware implements MiddlewareInterface
{

    Use JWT;



    /**
     * @param string $token
     * @param string $key
     * @return bool|
     */
    private function verification(string $token, string $key)
    {
        try {
            if ($this->jwsVerifier($token, $key))
                return true;
            return false;
        } catch (Exception $e) {
            return false;
        }
    }



    /**
     * Handles a request and produces a response.
     *
     * May call other collaborating code to generate the response.
     * @return mixed
     * @throws Exception
     */
    public function handle()
    {
        $jwt = $this->verification(Header::getBearerAuthToken(), env("SECRET_KEY"));
        if ($jwt) {
            return true;
        }
        Message::error(
            $this->getLpm()->translate('error', 'err_invalid_access_token'),
            RestCode::ERR_INVALID_ACCESS_TOKEN
        );
    }
}
