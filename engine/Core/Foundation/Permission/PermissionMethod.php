<?php

namespace Engine\Foundation\Permission;



use App\Model\Permission\Permission;
use App\Model\Role\Role;
use Engine\Core\LPManager\LPManager;
use Engine\Helper\Di;
use Engine\Helper\Message;



/**
 * Trait PermissionMethod
 * @package Engine\Foundation\User\Role
 */
trait PermissionMethod
{


    public function checkPermissionEdit()
    {
        $this->checkPermission($this->getRole($this->getUserEmail()), Action::EDIT);
    }



    public function checkPermissionView()
    {
        $this->checkPermission($this->getRole($this->getUserEmail()), Action::VIEW);
    }



    public function checkPermissionGet()
    {
        $this->checkPermission($this->getRole($this->getUserEmail()), Action::GET);
    }



    public function checkPermissionAdd()
    {
        $this->checkPermission($this->getRole($this->getUserEmail()), Action::ADD);
    }



    public function checkPermissionDelete()
    {
        $this->checkPermission($this->getRole($this->getUserEmail()), Action::DELETE);
    }



    /**
     * @param Role|null $role
     * @param $action
     * @return bool
     */
    public function checkPermission(?Role $role, $action)
    {
        $permissions_array = $role->getPermissions();
        foreach ($permissions_array as $permissions) {
            /** @var Permission $permissions */
            if ($permissions->getAction() == $action) {
                return true;
            }
        }
        Message::warning($this->lpm()->translate('action', "action_allowed_" . strtolower($action)));
    }



    private function lpm(): ?LPManager
    {
        return Di::get('lpm');
    }
}
