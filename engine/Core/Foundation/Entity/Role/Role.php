<?php

namespace Engine\Foundation\Entity\Role;



use Doctrine\Common\Collections\ArrayCollection;



/**
 * Trait Role
 * @package Engine\Foundation\Entity\Role
 */
trait Role
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Model\Permission\Permission", inversedBy="id")
     */
    private $permissions;


    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }



    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }



    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getPermissions()
    {
        return $this->permissions;
    }
}
