<?php

namespace Engine\Foundation\JWT;



use Engine\Helper\Header;
use Exception;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\Converter\StandardConverter;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\HS512;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;



/**
 * Trait JWTData
 * @package Engine\Foundation\JWT
 */
trait JWTData
{


    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return @$this->getPeyLoad()->email;
    }



    /**
     * @return int
     */
    public function getUserId(): int
    {
        return @$this->getPeyLoad()->id;
    }



    /**
     * @return bool|mixed
     */
    private function getPeyLoad()
    {
        return $this->getJWTData(Header::getBearerAuthToken(), env("SECRET_KEY"));
    }



    /**
     * @param string $token
     * @param string $secret_key
     * @return bool|mixed
     */
    private function getJWTData(string $token, string $secret_key)
    {
        try {
            $algorithmManager = AlgorithmManager::create([new HS512(),]);
            $jwsVerifier = new JWSVerifier($algorithmManager);
            $jwk = new JWK([
                'kty' => 'oct',
                'k' => $secret_key
            ]);
            $jsonConverter = new StandardConverter();
            $serializerManager = JWSSerializerManager::create([new CompactSerializer($jsonConverter),]);
            $jws = $serializerManager->unserialize($token);
            $isVerified = $jwsVerifier->verifyWithKey($jws, $jwk, 0);
            if ($isVerified)
                return json_decode($jws->getPayload());
            return false;
        } catch (Exception $e) {
            return false;
        }
    }

}
