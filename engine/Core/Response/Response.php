<?php


namespace Engine\Core\Response;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class Response
 * @package Engine\Core\Response
 */
class Response implements IResponse
{
    private $encoders = [];

    /**
     * @var JsonResponse
     */
    private $JsonResponse = null;
    /**
     * @var \Symfony\Component\HttpFoundation\Response
     */
    private $Response;

    /**
     * @var Serializer
     */
    private $serializer;




    /**
     * Response constructor.
     */
    public function __construct()
    {
        $this->encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $this->encoders);

        $this->Response = new \Symfony\Component\HttpFoundation\Response();

    }



    /**
     * @param       $data
     * @param int   $status
     * @param array $headers
     */
    public function jsonSend($data, $status = 200, $headers = []): void
    {
        $this->Response->setStatusCode($status);
        $this->Response->setContent($data);
        $this->Response->headers->set('Content-Type', 'application/json');
        $this->setHeaders($headers);
        $this->Response->send();
    }



    /**
     * @param       $data
     * @param int   $status
     * @param array $headers
     *
     * @return mixed
     */
    public function xmlSend($data, $status = 200, $headers = [])
    {
        $this->Response->setStatusCode($status);
        $this->Response->setContent($data);
        $this->Response->headers->set('Content-Type', 'application/xml');
        $this->setHeaders($headers);
        $this->Response->send();
    }



    /**
     * @param array $headers
     */
    private function setHeaders(array $headers)
    {
        foreach ($headers as $key => $value){
            $this->Response->headers->set($key, $value);
        }
    }
}
