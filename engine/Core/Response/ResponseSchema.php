<?php

namespace Engine\Core\Response;



/**
 * ------------------------------------------------------------------------------------
 *  Designed to build the structure before the object is serialized
 * ------------------------------------------------------------------------------------
 * Class ResponseModel
 * @package App\Object
 */
class ResponseSchema
{


    /**
     * @var int
     */
    public $code = 0;

    /**
     * @var int
     */
    public $count = 0;

    /**
     * @var array
     */
    public $items = [];


    /**
     * @var \stdClass
     */
    public $data;



    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }



    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }



    /**
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }



    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }



    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }
}
