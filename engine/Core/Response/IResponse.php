<?php


namespace Engine\Core\Response;


interface IResponse
{
    /**
     * @param       $data
     * @param int   $status
     * @param array $headers
     *
     * @return mixed
     */
    public function jsonSend($data, $status = 200, $headers = []);



    /**
     * @param       $data
     * @param int   $status
     * @param array $headers
     *
     * @return mixed
     */
    public function xmlSend($data, $status = 200, $headers = []);
}
