<?php
namespace Engine\Service\Orm;

use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Engine\Core\Config\Config;
use Engine\Service\AbstractProvider;
use Exception;



/**
 * Class Provider
 * @package Engine\Service\orm
 */
class Provider extends AbstractProvider
{


    /**
     * @return mixed
     * @throws Exception
     */
    public function init()
    {
        $dev_mode = env("DEV_MODE", true);

        if ($dev_mode == "dev") {
            $cache = new ArrayCache;
        } else {
            $cache = new ApcCache;
        }

        try {

            $conn = Config::groupByPath("database", "connections.mysql");
            $dir_models = Config::item("models", "orm");
        } catch (Exception $e) {
            die($e->getMessage());
        }

        $config = Setup::createAnnotationMetadataConfiguration($dir_models, $dev_mode, null, $cache, false);

        $this->di->set("em", EntityManager::create($conn, $config));
    }
}
