<?php


namespace Engine\Service\Response;


use Engine\Core\Response\Response;
use Engine\Service\AbstractProvider;

/**
 * Class Provider
 * @package Engine\Service\Response
 */
class Provider extends AbstractProvider
{

    /**
     * @return mixed
     */
    function init()
    {
        $this->di->set('response', new Response());
    }
}
