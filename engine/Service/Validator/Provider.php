<?php

namespace Engine\Service\Validator;



use Engine\Service\AbstractProvider;
use Rakit\Validation\Validator;



class Provider extends AbstractProvider
{


    /**
     * @return mixed
     */
    function init()
    {
        // see: https://github.com/rakit/validation
        $this->di->set("validator", new Validator);
    }
}
