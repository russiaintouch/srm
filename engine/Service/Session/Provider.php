<?php


namespace Engine\Service\Session;


use Engine\Core\Session\Session;
use Engine\Core\Session\SessionHandler;
use Engine\Service\AbstractProvider;

/**
 * Class Provider
 * @package Engine\Service\Session
 */
class Provider extends AbstractProvider
{

    /**
     * @return mixed
     */
    function init()
    {
        // $handler = new SessionHandler();
        // session_set_save_handler($handler, true);
        // session_start();
        //
        // $this->di->set('session', new Session());
    }
}
