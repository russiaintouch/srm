<?php

namespace Engine\Service\Passport;



use Engine\Core\Passport\Passport;
use Engine\Service\AbstractProvider;



class Provider extends AbstractProvider
{

    protected $serviceName = "passport";

    /**
     * @return mixed
     */
    function init()
    {
        $this->di->set($this->serviceName, new Passport(env("SECRET_KEY")));
    }
}
