<?php

namespace Engine\Service\Middleware;



use Engine\Core\Middleware\Middleware;
use Engine\Service\AbstractProvider;



/**
 * Class Provider
 * @package Engine\Service\Middleware
 */
class Provider extends AbstractProvider
{


    protected $serviceName = 'middleware';

    /**
     * @return mixed
     */
    function init()
    {
        $this->di->set($this->serviceName, new Middleware());
    }
}
