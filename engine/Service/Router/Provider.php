<?php

namespace Engine\Service\Router;


use Engine\Core\Config\Config;
use Engine\Service\AbstractProvider;
use Engine\Core\Router\Router;

/**
 * Class Provider
 */
class Provider extends AbstractProvider
{

    /**
     * @var string
     */
    public $serviceName = 'router';

    /**
     * @return mixed|void
     * @throws \Exception
     */
    public function init()
    {
        $router = new Router(env('HTTP_CATALOG', $_SERVER["HTTP_HOST"]));
        $this->di->set($this->serviceName, $router);
    }
}
