<?php

use Engine\DI\DI;



/**
 * @param DI $di
 * @param string $path
 * @throws Exception
 */
function initServiceProvider(DI $di, string $path)
{
    if (!empty($path)) {
        foreach (require $path as $service) {
            $provider = new $service($di);
            try {
                $provider->init();
            } catch (Exception $e) {
                throw new Exception("Failed to load the service [$service]");
            }
        }
    } else {
        throw new Exception("Provider directory not found!");
    }
}



/**
 * Получить директорию окружения Admin
 * @param string $section
 * @return string
 */
function path_admin($section = '')
{
    $pathMask = $_SERVER['DOCUMENT_ROOT'] . DS . 'admin/%s';
    switch (strtolower($section)) {
        case 'plugins':
            return sprintf($pathMask, 'Plugins');
        default:
            return $_SERVER['DOCUMENT_ROOT'] . DS . 'admin';
    }
}



/**
 * Returns list languages
 *
 * @return array
 */
function languages()
{
    $directory = path('language');
    $list = scandir($directory);
    $languages = [];
    if (!empty($list)) {
        unset($list[0]);
        unset($list[1]);
        foreach ($list as $dir) {
            $pathLangDir = $directory . DS . $dir;
            $pathConfig = $pathLangDir . '/config.json';
            if (is_dir($pathLangDir) and is_file($pathConfig)) {

                $config = file_get_contents($pathConfig);
                $info = json_decode($config);
                $languages[] = $info;
            }
        }
    }
    return $languages;
}



/**
 * Return list themes.
 *
 * @return array
 * @throws Exception
 */
function getThemes()
{
    $themesPath = '../content/themes';
    $list = scandir($themesPath);
    $baseUrl = Config::item('HTTP_CATALOG');
    $themes = [];
    if (!empty($list)) {
        unset($list[0]);
        unset($list[1]);
        foreach ($list as $dir) {
            $pathThemeDir = $themesPath . '/' . $dir;
            $pathConfig = $pathThemeDir . '/theme.json';
            $pathScreen = $baseUrl . '/content/themes/' . $dir . '/screen.jpg';
            if (is_dir($pathThemeDir) && is_file($pathConfig)) {
                $config = file_get_contents($pathConfig);
                $info = json_decode($config);
                $info->screen = $pathScreen;
                $info->dirTheme = $dir;
                $themes[] = $info;
            }
        }
    }
    return $themes;
}



/**
 * Return list plugins.
 * @param string $env
 * @return array
 */
function getPlugins($env = '')
{
    global $di;
    if ($env == '')
        $env = ENV;
    switch ($env) {
        case 'Admin':
            $pluginsPath = path_admin('plugins');
            break;
        case 'Cms':
            $pluginsPath = path_content('plugins');
            break;
        default:
            $pluginsPath = path_content('plugins');
    }
    $list = scandir($pluginsPath);
    $plugins = [];
    if (!empty($list)) {
        unset($list[0]);
        unset($list[1]);
        foreach ($list as $namePlugin) {
            $namespace = '\\Plugin\\' . $namePlugin . '\\Plugin';
            if (class_exists($namespace)) {
                /** @var Engine\Plugin $plugin */
                $plugin = new $namespace($di);
                $plugins[$namePlugin] = $plugin->details();
            }
        }
    }
    return $plugins;
}



/**
 * Return list plugins.
 * @param $modelName
 * @return bool
 */
function getModel($modelName)
{
    global $di;
    return $di->get($modelName);
}



/**
 * @param string $file_name
 * @return string
 */
function extractFileExt(string $file_name): string
{
    /**
     * Класс SplFileInfo предлагает высокоуровневый объектно-ориентированный интерфейс к информации для отдельного файла.
     */
    $info = new SplFileInfo($file_name);
    $ext = $info->getExtension();
    unset($info);
    return $ext;
}



/**
 * @param $var
 * @return bool
 */
function isBool($var)
{
    if (!is_string($var))
        return (bool)$var;
    switch (strtolower($var)) {
        case 'true':
            return true;
        case 'false':
            return true;
        default:
            return false;
    }
}



/**
 * @param $var
 * @return bool|null
 */
function toBool($var)
{
    if (!is_string($var))
        return (bool)$var;
    switch (strtolower($var)) {
        case 'true':
            return true;
        case 'false':
            return false;
        default:
            return null;
    }
}



/**
 *
 * @param $var
 * @return bool|float|int|null
 */
function varCast($var)
{
    if (is_numeric($var)) {
        return (int)$var;
    } elseif (is_float($var)) {
        return (double)$var;
    } elseif (toBool($var)) {
        return toBool($var);
    } else {
        return $var;
    }
}



/**
 *
 * @param array $array
 *
 * @return array
 */
function cast(array $array)
{
    $arr = [];
    foreach ($array as $key => $val) {
        $arr[$key] = varCast($val);
    }
    return $arr;
}



/**
 * @param string $string
 *
 * @return bool
 */
function json_validate(string $string): bool
{
    // decode the JSON data
    $result = json_decode($string);
    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid // No error has occurred
            break;
        case JSON_ERROR_DEPTH:
            $error = 'The maximum stack depth has been exceeded.';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Invalid or malformed JSON.';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Control character error, possibly incorrectly encoded.';
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
            break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
            $error = 'One or more recursive references in the value to be encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
            $error = 'One or more NAN or INF values in the value to be encoded.';
            break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
            $error = 'A value of a type that cannot be encoded was given.';
            break;
        default:
            $error = 'Unknown JSON error occured.';
            break;
    }
    if ($error !== '') {
        return false;
    }
    // everything is OK
    return true;
}



if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}
if (!function_exists('env')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            return value($default);
        }
        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }
        return $value;
    }
}
function updateEnv($data = array())
{
    if (!count($data)) {
        return;
    }
    $pattern = '/([^\=]*)\=[^\n]*/';
    $envFile = __DIR__ . '/../.env';
    $lines = file($envFile);
    $newLines = [];
    foreach ($lines as $line) {
        preg_match($pattern, $line, $matches);
        if (!count($matches)) {
            $newLines[] = $line;
            continue;
        }
        if (!key_exists(trim($matches[1]), $data)) {
            $newLines[] = $line;
            continue;
        }
        $line = trim($matches[1]) . "={$data[trim($matches[1])]}\n";
        $newLines[] = $line;
    }
    $newContent = implode('', $newLines);
    file_put_contents($envFile, $newContent);
}



if (!function_exists('setStrLength')) {
    /**
     * Принудительно задать длину строки.
     *
     * @param string $str
     * @param int $length
     *
     * @return mixed
     */
    function setStrLength(string $str, int $length)
    {
        return mb_strimwidth($str, 0, $length);
    }
}
if (!function_exists('isEmailAddress')) {
    /**
     * Валидатор адреса электронной почты
     *
     * @param string $address
     *
     * @return string
     */
    function isEmailAddress(string $address)
    {
        $re = '/(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/m';
        return (boolean)preg_match($re, $address);
    }
}
if (!function_exists('numGenerator')) {
    /**
     *
     * @param int $length
     *
     * @return mixed
     *
     */
    function numGenerator(int $length = 4)
    {
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str = $str . mt_rand(0, 9);
        }
        return $str;
    }
}
if (!function_exists('utsToDateTime')) {
    /**
     *
     * @param int $unixtimestamp
     * @return mixed
     * @throws Exception
     */
    function utsToDateTime(int $unixtimestamp): DateTime
    {
        return date_timestamp_set(new DateTime(), $unixtimestamp);
    }
}
if (!function_exists('stringToBoolean')) {
    /**
     *
     * @param string $str
     * @return mixed
     * @throws Exception
     */
    function stringToBoolean(string $str): bool
    {
        if (strtolower($str) === "true") {
            return true;
        }
        return false;
    }
}
