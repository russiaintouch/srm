<?php

namespace Engine\DI;



use Psr\Container\ContainerInterface;



/**
 * Dependency injection
 * Class DI
 * @package Engine\DI
 */
class DI implements ContainerInterface
{


    /**
     * @var array
     */
    private $container = [];



    /**
     * @param string $key
     * @param        $value
     *
     * @return $this
     */
    public function set(string $key, $value)
    {
        $this->container[$key] = $value;
        return $this;
    }



    /**
     * @param string $key
     *
     * @return bool
     */
    public function get($key)
    {
        return $this->has($key);
    }



    /**
     * @param $key
     *
     * @return bool
     */
    public function has($key)
    {
        return isset($this->container[$key]) ? $this->container[$key] : null;
    }



    /**
     * @param       $key
     * @param array $element
     *
     * @return bool
     */
    public function push($key, $element = [])
    {
        if ($this->has($key) !== null) {
            $this->set($key, $element);
            return true;
        }
        if (!empty($element)) {
            $this->container[$key][$element[$key]] = $element['value'];
        }
    }


}
