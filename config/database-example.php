<?php

return [
    "connections" => [
        "mysql" => [
            "dbname" => env("DB_DATABASE"),
            "user" => env("DB_USERNAME"),
            "password" => env("DB_PASSWORD"),
            "host" => env("DB_HOST"),
            "driver" => env("DB_DRIVER"),
        ],

        "sqlite" => [
            "driver" => "sqlite",
            "database" => env("DB_DATABASE"),
            "prefix" => ""
        ]
    ]
];
