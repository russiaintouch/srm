<?php

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\Migrations\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Setup;
use Engine\Core\Config\Config;
use Symfony\Component\Console\Helper\HelperSet;

define('ENV', 'Console');

require_once 'engine/bootstrap.php';

try {
    $dbParams = Config::group('database');
    $connection = DriverManager::getConnection($dbParams);
} catch (DBALException $e) {
    die($e->getMessage());
} catch (Exception $e) {
    die($e->getMessage());
}

$paths = [ROOT_DIR.'/database/migrations'];
$dev_mode = env('DEV_MODE', true);

$config = Setup::createAnnotationMetadataConfiguration($paths, $dev_mode);
$config->setProxyNamespace('api/Model');


try {
    $entityManager = EntityManager::create($dbParams, $config);
} catch (ORMException $e) {
    die($e->getMessage());
}

$helper_set = new HelperSet([
    'em' => new EntityManagerHelper($entityManager),
    'db' => new ConnectionHelper($entityManager->getConnection()),
]);


$commands = [];

if ($helper_set instanceof HelperSet) {
    ConsoleRunner::run($helper_set, $commands);
}
