<?php


namespace Console\src\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



/**
 * Class Configurator
 * @package Console\src\Command
 * @method someMethod()
 */
class SecretKeyGenerator extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:generate-secret-key';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('The secret key generator in the .env configuration file')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command generates a secret key.')
        ;
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $key = base64_encode($this->generator()) . base64_encode($this->generator());
        $output->writeln([
            '',
            '================================================================================================',
            'Generated key: <info>' . $key.'</>',
            '================================================================================================',
            '',
        ]);

        updateEnv([
            'SECRET_KEY' => $key
        ]);
    }


    private function generator(string $prefix = '')
    {
        return password_hash(uniqid($prefix, true), PASSWORD_DEFAULT);
    }
}
