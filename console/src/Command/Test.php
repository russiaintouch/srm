<?php


namespace Console\src\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



/**
 * Class Configurator
 * @package Console\src\Command
 * @method someMethod()
 */
class Test extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:test';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Это тест')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Эта команда для теста')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }


}
