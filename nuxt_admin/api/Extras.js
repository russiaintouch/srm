import axios from "axios";
import {Api} from "./Api";


export class Extras extends Api {

    /**
     *
     * @param formData
     * @returns {Promise<void>}
     */
    static async add(formData) {
        let response = await axios.post(`${process.env.api}/extras.add`, formData, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },

        });
        return await response.data;
    }


    /**
     *
     * @param params {count, offset}
     * @returns {Promise<void>}
     */
    static async get(params) {
        let response = await axios.get(`${process.env.api}/extras.get`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params

        });
        return await response.data;
    }
}
