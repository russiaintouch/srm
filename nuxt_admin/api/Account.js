import axios from "axios";
import {Api} from "./Api";


export default class Account extends Api {

    static async getProfileInfo() {
        let response = await axios.get(`${process.env.api}/account.getProfileInfo`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            }
        });
        return await response.data;
    }
}
