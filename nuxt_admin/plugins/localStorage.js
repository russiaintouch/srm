import createPersistedState from 'vuex-persistedstate'

export default async ({store}) => {
    window.onNuxtReady(async () => {
        await createPersistedState({
            key: 'vuex',
            paths: []

        })(store)
    })
}
