/**
 * @return {string}
 */
function Unix_timestamp(t)
{
    let dt = new Date(t * 1000);
    let hr = dt.getHours();
    let m = "0" + dt.getMinutes();
    let s = "0" + dt.getSeconds();
    return hr + ':' + m.substr(-2) + ':' + s.substr(-2);
}

/**
 *
 * @param UNIX_timestamp
 * @returns {string}
 */
export function UnixToStr(UNIX_timestamp){
    let a = new Date(UNIX_timestamp * 1000);
    let months = ['Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря'];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    // let hour = a.getHours();
    // let min = a.getMinutes();
    // let sec = a.getSeconds();
    return date + ' ' + month + ' ' + year;
}


/**
 * @return {number}
 */
export function StrDateToUnix(strDate) {
    let year, month, day
    [year, month, day]  = strDate.split("-")
    console.log(year, month, day)
    return Math.round(new Date(year, month-1, day).getTime()/1000)
}


