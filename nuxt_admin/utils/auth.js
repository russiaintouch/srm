// https://github.com/auth0/node-jsonwebtoken
import {SECRET_KEY} from "../key";

export default function isAuth(token) {
    const jwt = require('jsonwebtoken');
    try{
        jwt.verify(token, Buffer.from(SECRET_KEY, 'base64'), {
            algorithms: ['HS512']
        });
        return true;
    }catch (e) {
        return false
    }
}
