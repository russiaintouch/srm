/**
 * Vuex
 *
 * @library
 *
 * https://vuex.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue'
import Vuex from 'vuex'
// Store functionality
import actions from './actions'
import getters from './getters'
import {mutations} from './mutations'
import {state} from './state'
import plugins from './plugins'

Vue.use(Vuex)

const store = () => new Vuex.Store({
    plugins,
    actions,
    getters,
    mutations,
    state
})


export default store
