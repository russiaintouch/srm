// https://vuex.vuejs.org/en/getters.html

import {M_PRODUCT} from "./mutatuin-types"

export default {

    [M_PRODUCT]: (state) => {
        return state[M_PRODUCT]
    },

}
