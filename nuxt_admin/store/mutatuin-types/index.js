const M_ID = "id"
const M_ACCESS_TOKEN = "access_token"
const M_PROFILE = "profile"
const M_EMAIL = "email"
const M_PRODUCT = "product"


module.exports = {
    M_ID,
    M_ACCESS_TOKEN,
    M_PROFILE,
    M_EMAIL,
    M_PRODUCT,
}
