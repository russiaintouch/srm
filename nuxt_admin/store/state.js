// https://vuex.vuejs.org/en/state.html

import {M_PRODUCT, M_PROFILE} from "./mutatuin-types";

export const state = () => ({
    [M_PROFILE]: {},
    [M_PRODUCT]: {}
})
