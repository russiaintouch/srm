
const changeLoaderOptions = loaders => {
    if (loaders) {
        for (const loader of loaders) {
            if (loader.loader === 'sass-loader') {
                Object.assign(loader.options, {
                    includePaths: ['./assets']
                })
            }
        }
    }
}


//const webpack = require('webpack');

module.exports = {
    mode: "spa", // spa

    env: {
        api: process.env.NODE_MODE === 'dev' ? 'http://api.srm.loc': 'http://api.srm.loc'
    },


    /*
    ** Headers of the page
    */
    head: {

        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Unofficial Nuxt + CoreUI project, free to use boilerplate for every need.'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    /*
    ** Customize the progress bar color
    */
    loading: {
        color: '#72a4b3',
        height: '2px'
    },

    /**
     * Import CSS
     */
    css: [
        /* Import Font Awesome Icons Set */
        '~/node_modules/flag-icon-css/css/flag-icon.min.css',
        /* Import Font Awesome Icons Set */
        '~/node_modules/font-awesome/css/font-awesome.min.css',
        /* Import Simple Line Icons Set */
        '~/node_modules/simple-line-icons/css/simple-line-icons.css',
        /* Import Bootstrap Vue Styles */
        '~/node_modules/bootstrap-vue/dist/bootstrap-vue.css',
        /* Import Core SCSS */
        {src: '~/assets/scss/style.scss', lang: 'scss'}
    ],


    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {src: '~/plugins/localStorage.js', ssr: false},
        {src: '~/plugins/nuxt-client-init.js', ssr: false},
        '~/plugins/axios',
        '~/plugins/vue-notification.js',
        '~/plugins/vee-validate.js',
        {src: '~/plugins/ckeditor5.js', ssr: false},
        // '~/plugins/vuetify.js',
        // '~/plugins/datatable.js',
    ],


    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        // Doc: https://github.com/bootstrap-vue/bootstrap-vue
        'bootstrap-vue/nuxt',
        [
            'nuxt-session',
            {
                // express-session options:
                name: 'nuxt-session-id',
                secret: 'DEVELOPER',
                cookie: {
                    maxAge: 1000 * 60 * 60 * 24 * 7 * 52 * 2 // 2 years
                },
                saveUninitialized: true,
                resave: true
            }
        ],
        '@nuxtjs/axios',
        '@nuxtjs/toast'
    ],

    /*
    ** Style resources configuration
    */
    styleResources: {
        scss: './assets/scss/style.scss'
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, {isDev, isClient}) {


            if (isDev) {
                config.devtool = 'eval-source-map'  // Something you like
            }


            if (isDev && isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })

                const vueLoader = config.module.rules.find(
                    ({loader}) => loader === 'vue-loader')
                const {options: {loaders}} = vueLoader || {options: {}}

                if (loaders) {
                    for (const loader of Object.values(loaders)) {
                        changeLoaderOptions(Array.isArray(loader) ? loader : [loader])
                    }
                }

                config.module.rules.forEach(rule => changeLoaderOptions(rule.use))
            }
        }
    },


    axios: {
        baseURL: 'http://api.srm.loc/',
        headers: {
            Env: "myvalue"
        }
    },

}
