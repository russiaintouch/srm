import isAuth from "../utils/auth";
import Cookie from "../utils/cookie";

/**
 * @param req
 * @param store
 * @param redirect
 * @returns {*}
 */
export default async function ({store, req, redirect}) {

    if (process.browser){

        console.log('check auth')
        let auth = isAuth(Cookie.get("access_token"))
        if(!auth){
            redirect('/login')
        }

    }
};

