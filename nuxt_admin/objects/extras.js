export let extras = {
    name: String(""),
    price: Number(0),
    limitation: Number(0),
    description: String(""),
    image: String(""),
}
