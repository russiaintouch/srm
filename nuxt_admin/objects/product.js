export let product = {
    productType: Number(0),
    name: String(""),
    price: Number(0),
    countMin: Number(1),
    countMax: Number(1),
    shortDescription: String(""),
    longDescription: String(""),
    dataType: Number(0),
    extras: Array()
}
