export const discountNew = {
    code: String(""),
    appliedOncePer: Number(0),
    validityFrom: String(""),
    validityTo: String(""),
    limiting: Number(0),
    minAmount: Number(0),
    taxesFees: Boolean(false),
}

export const discountEdit = {
    code: String(""),
    appliedOncePer: Number(0),
    validityFrom: String(""),
    validityTo: String(""),
    limiting: Number(0),
    minAmount: Number(0),
    taxesFees: Boolean(false),
}

