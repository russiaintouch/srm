import {isAuth} from "../utils/auth";
import Cookie from "../utils/cookie";

/**
 * @param req
 * @param store
 * @param redirect
 * @returns {*}
 */
export default async function ({store, req, redirect}) {

    if (process.browser) {
        console.info('check auth...')
        let auth = await isAuth(Cookie.get("access_token"), "127.0.0.1")

        if (!auth) {
            return redirect('/login')
        }
    }
};

