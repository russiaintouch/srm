// https://vuex.vuejs.org/en/mutations.html

import {M_PROFILE, M_PRODUCT} from "./mutatuin-types";

export const mutations = {

    [M_PROFILE]: (state, value) => {
        state[M_PROFILE] = value
    },

    [M_PRODUCT]: (state, value) => {
        state[M_PRODUCT] = value
    },

}
