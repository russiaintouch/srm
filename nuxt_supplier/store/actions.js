// https://vuex.vuejs.org/en/actions.html

const {ACCESS_TOKEN} = require("./mutatuin-types");

const cookieparser = process.server ? require('cookieparser') : undefined;

export default {

    async nuxtServerInit({dispatch,commit}, {req}) {

        console.log("nuxtServerInit...")

        let access_token = '';

        if (!req.session.access_token){
            req.session.access_token = ''
        }

        if (req.headers.cookie) {
            const parsed = cookieparser.parse(req.headers.cookie);
            try {

                access_token = (typeof parsed.access_token !== "undefined") ? parsed.access_token : '';
                await commit([ACCESS_TOKEN], access_token);
                req.session.access_token = access_token;

            } catch (err) {
                console.error(err);
            }
        }
    },

}
