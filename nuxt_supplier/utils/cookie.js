
import cook from "js-cookie";

export default class Cookie {

    /**
     *
     * @param key
     * @returns {*}
     */
    static get(key){
        return cook.get(key);
    }

    /**
     *
     * @param key
     * @param value
     */
    static set(key, value){
        cook.set(key, value)
    }

    /**
     *
     * @param key
     */
    static remove(key){
        cook.remove(key)
    }
}
