import Auth from "../api/Auth";

/**
 *
 * @param access_token
 * @returns {Promise<boolean>}
 */
export async function isAuth(access_token, ip) {
    let res = await Auth.checkToken(access_token, ip)
    if (res.code === 0) {
        return true
    }
    return false
}
