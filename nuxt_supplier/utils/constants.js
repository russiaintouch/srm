const PER_PAGE_ROWS = 10;
const TOAST_DURATION = 1500;

module.exports = {
    PER_PAGE_ROWS,
    TOAST_DURATION
};
