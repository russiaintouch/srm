import Vue from 'vue';
import DataTable from 'v-data-table'

//https://www.npmjs.com/package/v-data-table
Vue.use(DataTable);
