import Vue from 'vue';
import VeeValidate from 'vee-validate';

//https://baianat.github.io/vee-validate/
Vue.use(VeeValidate,  {
    locale: 'ru',
    inject: true,
    fieldsBagName: 'veeFields'
});
