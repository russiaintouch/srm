import isAuth from "../utils/auth";
import Cookie from "../utils/cookie";

export default (ctx) => {

    let auth = isAuth(Cookie.get("access_token"))

    if(!auth){
        ctx.redirect('/login')
    } else {
        ctx.redirect('/')
    }

}
