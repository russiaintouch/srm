import axios from "axios";

export default class Auth {

    /**
     * @param email
     * @param password
     * @returns {Promise<void>}
     */
    static async getAccessToken(email, password) {
        let response = await axios.get(`${process.env.api}/secure.getAccessToken`, {
            params: {
                email,
                password
            }
        });
        return await response.data;
    }


    /**
     * check access_token
     * @param token
     * @param ip
     * @returns {Promise<void>}
     */
    static async checkToken(token, ip) {
        let response = await axios.get(`${process.env.api}/secure.checkToken`, {
            params: {
                token,
                ip
            }
        });
        return await response.data;
    }
}
