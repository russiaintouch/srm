import Cookie from "../utils/cookie";


export class Api {

    static token() {
        return Cookie.get("access_token")
    }
}
