import axios from "axios/index";


export default class Registration {
    /**
     * Registration
     * ========================
     * @param formData
     * @returns {Promise<void>}
     */
    static async registration(formData) {
        let response = await axios.post(`${process.env.api}/account.registration`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        return await response.data;
    }


    /**
     * Confirmation of registration
     *
     * @param email
     * @param code
     * @returns {Promise<void>}
     */
    static async confirmation(email, code) {
        let response = await axios.get(`${process.env.api}/account.confirmation`, {
            params: {
                code,
                email
            }
        });
        return await response.data;
    }
}



