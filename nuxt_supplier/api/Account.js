import axios from "axios";
import {Api} from "./Api";


export default class Account extends Api {

    static async getProfileInfo() {
        let response = await axios.get(`${process.env.api}/account.getProfileInfo`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            }
        });
        return await response.data;
    }

    /**
     *
     * @param formData FormData
     * @returns {Promise<void>}
     */
    static async saveProfileInfo(formData) {
        let response = await axios.post(`${process.env.api}/account.saveProfileInfo`, formData,
            {
                headers: {
                    "Authorization": `Bearer ${this.token()}`
                }
            });
        return await response.data;
    }
}
