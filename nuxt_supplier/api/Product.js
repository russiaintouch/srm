import axios from "axios";
import {Api} from "./Api";


export class Product extends Api{

    /**
     *
     * @param count
     * @param offset
     * @returns {Promise<void>}
     */
    static async getProductType(count = 30, offset = 0) {
        let response = await axios.get(`${process.env.api}/product.getProductType`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params: {
                count,
                offset
            }
        });
        return await response.data;
    }


    /**
     *
     * @param  id
     * @returns {Promise<void>}
     */
    static async getById(id) {
        let response = await axios.get(`${process.env.api}/product.getById`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params: {
                id
            }
        });
        return await response.data;
    }


    /**
     *
     * @returns {Promise<void>}
     * @param params {q, count, offset}
     */
    static async getProducts(params) {
        let response = await axios.get(`${process.env.api}/product.getProducts`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params
        });
        return await response.data;
    }


    /**
     *
     * @param formData
     * @returns {Promise<void>}
     */
    static async add(formData) {
        let response = await axios.post(`${process.env.api}/product.add`,
            formData, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            }
        });
        return await response.data;
    }


    /**
     *
     * @param id
     * @returns {Promise<void>}
     */
    static async delete(id) {
        let response = await axios.get(`${process.env.api}/product.delete`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params: {
                id
            }
        });
        return await response.data;
    }
}
