import axios from "axios";
import {Api} from "./Api";


export class Category extends Api{


    /**
     *
     * @param params {count, offset}
     * @returns {Promise<void>}
     */
    static async get(params) {
        let response = await axios.get(`${process.env.api}/category.get`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params

        });
        return await response.data;
    }


    /**
     *
     * @param id
     * @returns {Promise<void>}
     */
    static async delete(id) {
        let response = await axios.get(`${process.env.api}/category.delete`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params: {
                id
            }
        });
        return await response.data;
    }


    /**
     *
     * @param id
     * @returns {Promise<void>}
     */
    static async getById(id) {
        let response = await axios.get(`${process.env.api}/category.getById`, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            },
            params: {
                id
            }
        });
        return await response.data;
    }


    /**
     *
     * @param formData
     * @returns {Promise<void>}
     */
    static async edit(formData) {
        let response = await axios.post(`${process.env.api}/category.edit`, formData, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            }
        });
        return await response.data;
    }


    /**
     * @param formData
     * @returns {Promise<void>}
     */
    static async add(formData) {
        let response = await axios.post(`${process.env.api}/category.add`, formData, {
            headers: {
                "Authorization": `Bearer ${this.token()}`
            }
        });
        return await response.data;
    }
}
